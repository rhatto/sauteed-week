# Sauteed Week API Backend

During the [Tor Hackweek][] 2022 a sample API was coded to show the potential
of the technology.

This document deals on how to install and test it.

## Requirements

The following dependencies are needed:

* [Git][]
* [GNU Make][], for project-level tasks
* Either:
    * Using Docker: [Docker][] and [Docker Compose][] OR
    * Using Python directly: [Python 3][] and [Pipenv][]

You also need a copy of the [Sauteed Week][] source code:

    git clone https://gitlab.torproject.org/rhatto/sauteed-week.git

Example installation scripts are provided at the [bin][] folder of the [Sauteed
Week][] repository and assume a fresh [Debian][]-based system, but can be
adapted for other environments.

## Using Docker

With the requirements met, just run the following command in the top-level
folder of the [Sauteed Week][] repository:

    make run-containers

To watch the container logs, type

    make log-containers

The following containers are started:

* `api`: runs the [Sauteed Week][] API on [http://localhost:8100][].
* `docs`: serves these docs on [http://localhost:8000][].
* `tor`: serves both the `api` and `docs` via [Tor Onion Services][]

Use the following commands to get the Onion Service address for the API backend
and docs:

    docker exec -it sauteed-week_tor_1 cat /var/lib/tor/api/hostname
    docker exec -it sauteed-week_tor_1 cat /var/lib/tor/docs/hostname

## Using Python

With the requirements met, just run the following command in the top-level
folder of the [Sauteed Week][] repository:

    make vendor
    make api

## Usage

With the [Sauteed Week][] API running, point your favourite HTTP client
to http://localhost:8100][].

Example invocations using `curl`:

    curl "http://localhost:8100/search?in=www.sauteed-onions.org"
    curl "http://localhost:8100/get?id=5957691193"
    curl "http://localhost:8100/get-certificate?id=5957691193"

[Debian]: https://www.debian.org
[Git]: https://git-scm.com/
[GNU Make]: https://www.gnu.org/software/make
[Tor Hackweek]: https://gitlab.torproject.org/tpo/community/hackweek
[Docker]: https://docs.docker.com
[Docker Compose]: https://docs.docker.com/compose
[Python 3]: https://www.python.org
[Pipenv]: https://docs.pipenv.org
[Sauteed Week]: https://gitlab.torproject.org/rhatto/sauteed-week
[Tor Onion Services]: https://community.torproject.org/onion-services/
[bin]: https://gitlab.torproject.org/rhatto/sauteed-week/-/tree/main/bin
[http://localhost:8100]: http://localhost:8100
[http://localhost:8000]: http://localhost:8000
