# A Sauteed Week in 5 minutes

# Intro

What we want:

    somename.in.a.tld ->
    test35n4rit2dzagyzixi7kfktuzns3q464donfggtn5jhflqvwihrqd.onion

What would be great (bi-directionality):

    somename.in.a.tld <->
    test35n4rit2dzagyzixi7kfktuzns3q464donfggtn5jhflqvwihrqd.onion

What already exists:

0. The overall design proposal and API specs.
1. A CT Log monitoring backend.
2. A WebExtension.

# Roadmap

See [Roadmap](/info/#roadmap).

# Our team

    23:51 Users #sauteed-onions:
    23:51  _syverson  dkg           kwadronaut  micah  rgdd
    23:51  arma2      hackerncoder  ln5         raya   rhatto
    23:51 --- Irssi: #sauteed-onions: 10 nicks (@/0 +/0  -/0)

# Deliverables

What we've done during the week:

1. Documentation!
2. Sauteed Week API.
3. Testing of the existing (backend and frontend implementations).
4. Most importantly: interesting discussions happened :)

GitLab project gathering this work:
[https://gitlab.torproject.org/rhatto/sauteed-week](https://gitlab.torproject.org/rhatto/sauteed-week)

# Demonstrations

* The sauteed onion certificate for [autodefesa.org](https://autodefesa.org).
* Documentation!
* Sauteed Week API.

# Applications

Some applications of the technology (see the [Applications](/applications) page
for discussion on problem spaces and goals):

## 1. DNS to Onion Service address translation

* That's the immediate application when a sauteed onion certificate is
  deployed.
* Status: implemented.

## 2. Certificate-based Onion-Location

* Requires a WebExtension at the Tor Browser.
* Status: PoC/prototype implemented; needs work, enhancements and eventual
  integrationg into the current Tor Browser Onion Location widget.
* Demonstration:
   * Sauteed Onions WebExtension:
       * Open https://torproject.org with the developmen Firefox to show that
         there's no Onion-Location (since we're not using Tor Browser).
       * Then open domains that uses sauteed onions certificates and show the
         redirection (including how the webext respects the path).

## 3. Search engine

* Built a robust search engine that cannot lie about (non-)answers to
  questions about onion addresses for domain names without detection.
* Status: in concept.

## 4. Onion Service Key Rotation

* Would allow operators to rotate Onion Service keys (independently or in
  addition to a future implementation of offline master keys for Onion
  Services).
* Requires bi-directionality in the `.onion addr <-> DNS name` association.

## 5. Alternative load balancing mechanism

* Multiple .onion addresses could be included as sauteed onion Subject
  Alternative Names in the TLS certificates, allowing some additional
  redundancy/failover/balancing (which is non-conflicting with Onionbalance
  usage).

# Architecture

* Next iteration of the API:

* For a (hopefully) near future, it should be possible to plug the monitor
  code into some database and then change the API backend to consume it.
* Perhaps the monitor could be refactored to do fetches in parallel and be
  installed in some virtual machine, to have a development/test API
  available).

[Sketch][] for a proposed next-step API service:

                                                                 ┌─────► Search engines
                                                                 │
                                                                 │
        CT Logs────► .onion Monitors────► Databases───► APIs────►│
                                                                 │
                                                                 │
                                                                 └─────► Resolvers


[Sketch]: https://asciiflow.com/#/share/eJyrVspLzE1VssorzcnRUcpJrEwtUrJSqo5RqohRsrI0M9OJUaoEsowsDYCsktSKEiAnRkmBXPBoyh50NG2XQnBqYlFyhkJqXnpmXmpxTEweJeZTrt05BEj75KcXo7tTLz8vMz9PwRdIleQXYUi7JJYkJiUWpxajiDoGeGJ6mSruHEDtWCIxKLU4P6cstag4RqlWqRYAAUixVA%3D%3D)

## Discussion highlights

* DNSSEC versus CT Logs.

* It's important to make sure that the resolver is not lying when it tells
  that there's no entry for a given name? It's known how to solve that with both
  CA/CT and DNSSEC even though the solutions differ on a technical level.

* Ideally, the trust required to run one of all these nodes would be equivalent
  to those running a Tor relay:

# Questions

1. What is the number of sites currently with the Onion Location Header?
   And the number of sites with *both* an onion address *and* a non-onion address?

2. How large would be the Sauteed Onion data set? How it would grow?

3. How to scale monitoring, storage (of the sauteed database) and queries?

4. Could resolvers be part of Tor's DHT?

5. Or could resolvers be part of The Tor Browser Bundle (delivering the data
   set to be queried locally, similar to Onion Names).

# Further steps

1. Include some additional notes and
   [wrap up the Sauteed Week repository](https://gitlab.torproject.org/rhatto/sauteed-week/-/issues/8).

2. Suggestion for the Sauteed Onions initiative talk with the CAs about the
   idea: at least have a dialog with Let's Encrypt before recommending any broad
   usage.

3. Task/content triage from what happened in the Sauteed Week as an input to the Sauteed
   Onions initiative (like leaving all this content available to be included on upstream's
   site, at least what makes sense!).
