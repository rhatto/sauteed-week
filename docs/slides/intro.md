---
title: Onion Service address discovery using Sauteed Onions
subtitle: Tor Hackweek 2022
date: 2022-06-27
fontsize: 8pt
---

# Biggest Onion Services UX Challenge?

Translate memorable and meaningful names into Onion Service addresses:

    somename.in.a.tld ->
    test35n4rit2dzagyzixi7kfktuzns3q464donfggtn5jhflqvwihrqd.onion

where `a.tld` would either be an existing top-level domain name or could be a
new special-use top-level (like what [RFC 7686](https://datatracker.ietf.org/doc/html/rfc7686)
does).

# Some existing proposals

To name a few:

* Rule-based Onion Names: HTTPS-Everywhere (already on Tor Browser Bundle).
* Blockchain based, such as Namecoin.
* Certificate Transparency (CT) Logs: Sauteed Onions.

Source:

* https://gitlab.torproject.org/tpo/onion-services/onion-support/-/wikis/Documentation/Onion-Services-UX-Proposals
* https://gitlab.torproject.org/tpo/onion-services/onion-support/-/issues/64

# Sauteed Onions

* This Hackweek proposal is focused on Sauteed Onions.

* It's a mechanism to translate between DNS names and .onion addresses using
  Certificate Transparency (CT) Logs.

* https://www.sauteed-onions.org

# Example

Naming things:

* www.torproject.org DNS name
* 2gzyxa5ihm7nsggfxnu52rck2vv4rvmdlkiu3zzui5du4xyclen53wid.onion address
* sauteed onion is 2gzyxa5ihm7nsggfxnu52rck2vv4rvmdlkiu3zzui5du4xyclen53widonion.www.torproject.org

Setup:

* System administrators "publishes" associations between DNS domains and .onion
  addresses by DNS entries and creating valid TLS certificates including the
  sauteed onion as an Subject-Alt-Name (SAN - alternative address).

* Every issued certificate goes to a distributed append-only Certificate
  Transparency log: https://certificate.transparency.dev/howctworks/

# User histories

A possible flow in the Tor Browser:

1. User requests `site.in.a.td`.
2. Tor Browser checks a "resolver" service for a Sauteed Onion matching this domain.
3. Onion-Location button or automatic redirection to the .onion address.

# Advantages

* Low implementation cost: just a DNS and a SAN certificate entry.
* Compatible with Let's Encrypt / ACME / Certbot.
* Can be deployed today!

# Current status

* Sauteed Onions domains can already be deployed.
* Backend resolver/API and web extension still under development.
* Reference implementation: https://gitlab.torproject.org/rgdd/sauteed-onions

# Ideas for the Hackweek

* Test Sauteed Onions on community-provided domains.
* Design, basic implementation and/or test new or existing tools like:
  * A backend resolver API behind Onion Services.
  * A Tor Browser Bundle add-on that queries a Sauteed Onions resolver as an
    alternative to the Onion-Location Header or something like that.

# Needed skills

What we'll do really depends on the aggregated skills in the team, such as:

* System administration skills.

* Coding skills depending on the tools to be developed.

* UX skills.

* People with experience with CT Logs :)

# Open questions

1. How such a system could scale if querying happens
   every time an user asks for a site and if the request goes through an API
   served via a set of Onion Service "resolvers", each one configured using
   something like Onionbalance for additional performance and failover?

2. How much such infrastructure would cost in terms of resources
   and maintenance?

3. How that backend architecture would look like?

# Questions?

* https://pad.riseup.net/p/tor-hackweek-2022-sauteed-onions-keep
* #sauteed-onions at OFTC
