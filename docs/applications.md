# Applications

All of these approaches sounds (initially) non-conflicting and compatible.

Some of them (and others) are already covered at the [Sauteed Onions draft
paper].

[Sauteed Onions draft paper]: https://www.sauteed-onions.org/doc/paper.pdf

## Problem spaces

Before talking about each application, let's first try to organize the
discussion in terms of goals and problems solved by each approach/application.

It would be a meaningful activity to think about what that is supposed to
achieve, like what are the goals / problems solved.

Thinking out loud:

* Is it to have a chance at automatically finding the onion site if the regular
  site is offline (censorship/UX angle)?

* Is it to not touch the regular site at all and always get to the onion site
  (traffic analysis angle)?

* Is it to make it easier to automate maintenance of a list ("just add the
  domain name to the list, discovery of its onion address is then automatic
  when we build the data set")?

The spaces:

1. Human-readable names.

2. Censorship resistance:
   * Types of censorship:
    * IP-based.
    * Domain-based.
  * Scale of censorship:
    * Local (limited to a country or an even smaller network space).
    * Global events (domain seizure?). How global censorship are currently (if)
      defined?

3. Onion Service enumeration.

As an additional reference when comparing proposals, check the [Onion Services
UX Proposals][] document.

### Zoomed-out discoverability goal

Clients can query a third-party service for some record that has the semantics
"`www.example.com`'s onion service is `<addr>.onion`", and the answer ought to be
**(a)** cryptographically verifiable against some other authority that the binding
is correct before using it (like a CA), and **(b)** have some amount of
cryptographically-strong "no such binding" response.

(E.g., a robust search engine is a frequent wish by users, see [sec18-winter][])

[sec18-winter]: https://www.usenix.org/system/files/conference/usenixsecurity18/sec18-winter.pdf)

### Zoomed-out censorship goal

Blocking `www.example.com` should not make it difficult to access the onion
service of `www.example.com`.

(E.g., onion location does not work if a site becomes censored.)

### Misc goal related to anonymity

Ensure that clients use the same onion services for `www.example.com` to keep the
anonymity set as large as possible.

(E.g., onion redirects could be targeted.)

[Onion Services UX Proposals]: https://gitlab.torproject.org/tpo/onion-services/onion-support/-/wikis/Documentation/Onion-Services-UX-Proposals

## Requirements

### The sauteed onion criteria

* It is crucial to require "`SAN: <addr>onion.example.com`" AND "`example.com`"
  to mitigate the threat of dangerous labels (see [draft-dkg-intarea-dangerous-labels][]).
* Ideally "`<addr>onion.example.com`" would be in a `X509v3` extension; using a
  subdomain is a hack that makes it easy to deploy in the short-term ("now").
  If the `X509v3` extension is available, setting up sauteed onions won't require
  creating additional DNS records.
* At least have a dialog with Let's Encrypt before recommending any broad usage.
* To get discovery property **(b)**, the sauteed onion data set that is downloaded
  from CT logs needs to be represented as a data structure with efficient
  non-membership proofs by the API provider. I.e., it is not achieved by "just
  doing crt.sh alone".
    * See [Sparse Merkle tree][].
    * See [Verifiable log-backed map][].
    * Without this we loose properties in the "Misc goal related to anonymity"
      above.

[Sparse Merkle tree]: https://eprint.iacr.org/2016/683.pdf
[Verifiable log-backed map]: https://github.com/google/trillian/blob/master/docs/papers/VerifiableDataStructures.pdf
[draft-dkg-intarea-dangerous-labels]: https://datatracker.ietf.org/doc/draft-dkg-intarea-dangerous-labels/

## DNS to Onion Service address translation

This seems like the most immediate application: translate memorable and
meaningful DNS-based names into Onion Service addresses:

    somename.in.a.tld ->
    test35n4rit2dzagyzixi7kfktuzns3q464donfggtn5jhflqvwihrqd.onion

It's important to note that any automatic querying is completely uncharted
territory at this point, mainly because of the engineering rodmap needed
to have such system scalable an running on production.

## Certificate-based Onion-Location

With Sauteed Onions, [Onion Location] could also be based on a certificate
(served by a web server) rather than an HTTP header (also served by the web
server), and the type of redirection is then of the same character; "whatever
the web server's operator specified".

It's important to note that both approaches are non-conflicting: site operators
can even use both at the same time: the Onion Location HTTP Header and the
sauteed onion certificate.

In other words, having a sauteed onion certificate does not conflict with the
Onion-Location header.

The WebExtension could have some logic of what to prioritize and depending on
what is available for each site.

Some things to note:

1. Putting onion-location in cert rather than http header, one feature of having
   it in the http header is that the onion-location can have.
2. A full url, i.e. to bring you from foo.com/a/b to foo.onion/a/b.
3. You can see that difference in user experience right now, where
   torproject.org serves a page-specific onion-location header, whereas
   twitter.com just serves a generic "here is our onion" header (twitter does it
   that way because they're actually using Cloudflare's "which country is it, oh
   it is tor country" hack to inject that header).

So an alternative, certificate based Onion Location could, depending on UX
choices and implementation constraints, redirect based on the current request
URL and not just to the onion address without the full path.

Advantages of the certificate-based Onion Location over the HTTP-header one:

1. The former could work for protocols other than HTTPS.
2. The former does not suffer from faulty implementations of the late when
   the Onion-Location is served over HTTP instead of HTTPS connections, but that
   should not be an advantage at all if the implementation forces HTTPs
   when accessing traditional (non-onion) sites.

[Onion Location]: https://community.torproject.org/onion-services/advanced/onion-location/

## Search engine

Sauteed Onions allows a concise view of who may redirect to what (opt-in to set
this up, of course).

And from this we can build a robust search engine that cannot lie about
(non-)answers to questions about onion addresses for domain names without
detection.

* This may be one of the most interesting feature that could add something
  compared to ad-hoc DuckDuckGo, looking for manually curated lists, etc.
  By itself this application can stand on it's own feet.
* It doesnt have to be in tb or a webext - could e.g. be on a site similar to
  crt.sh.
* The benefit of allowing such search more natively is that verification can be
  automatic (which is otherwise skipped, introducing a trust boundary between
  the user and the api).

People are doing that now in ad-hoc ways; they are not robust, hard to
automate, and involing blind trust.

## Bi-directional associations

Sauteed Onions certificates builds unidirectional associations between a
DNS-based domain name and an Onion Service address:

    somename.in.a.tld ->
    test35n4rit2dzagyzixi7kfktuzns3q464donfggtn5jhflqvwihrqd.onion

Bi-directionality could be achieved by having the TLS certificate signed by
the Onion Service private key and published in a [well-known][] folder (which
could be available either via onion service or just regular HTTP(S). Some
automation is needed to make this process practical.

Bi-directionality allows clients to check whether the domain/cert operator are
really in control of the onion service.

    somename.in.a.tld <->
    test35n4rit2dzagyzixi7kfktuzns3q464donfggtn5jhflqvwihrqd.onion

All other applications could benefit from this bi-directional association.

See also [SATAs discussion][].

[SATAs discussion]: https://arxiv.org/pdf/2110.03168.pdf

## Onion Service Key Rotation

Bi-directional associations could be a workaround (or a compliment when the
feature is implement) for [prop224 offline master keys][] (defined on section
"1.7. In more detail: Keeping crypto keys offline" of the [rend-spec-v3][]), as
this bidirectionality could allow service operators to rotate their Onion
Service keys by putting both the new and old public keys as sauteed onion
addresses (i.e, the association made into the TLS certificate) and using both
the old and new onion service private key to sign the sauteed onion
certificate.

This allows rotations procedures. The logic at the Tor Browser's WebExtension still
need to be defined for this idea: how to figure out which Onion Service address
to use?; etc.

What's important here is that the rotation is happening through the
cross-signing of both the TLS certificate and the Onion Service keys and without the
need of a Onion Service offline master key.

But in theory both approaches are compatible.

[well-known]: https://datatracker.ietf.org/doc/html/rfc5785
[prop224 offline master keys]: https://gitlab.torproject.org/tpo/core/tor/-/issues/29054
[rend-spec-v3]: https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/rend-spec-v3.txt

## Alternative load balancing mechanism

A similar reasoning for the Onion Service Key Rotation application may be used to attach
many onion service addresses into the same TLS certificate for a given domain:

    somename.in.a.tld <->
      <onion-address1>
      <onion-address2>
      <onion-address3>
      ...
      <onion-addressN>

Coupled with additional WebExtension logic at the Tor Browser could make
possible to randomly pick one of the onion service addresses, thus creating
some balancing similar to round-robin DNS entries.

A disadvantage of this application is that load balancing would depend on
client logic: non-compliant clients could ignore this setting and thus forcing
connection through one of the onion addresses.

This application is non-conflicting with [Onionbalance] and in theory could even
be used altogether.

[Onionbalance]: https://onionbalance.readthedocs.io/en/latest/

## Sauteed Onion to [Onion Name] translation

Having DNS to Onion Service address translation may be one step towards having
a dedicated special-use domain name such as `sauteed.onion`.

Then the WebExtension in the Tor Browser could accept queries such as
`somename.sauteed.onion`, query a resolver API and get the Onion Service
address.

But how to resolve conflicts between `somename.example.org` and
`somename.example2.org`?  Who get's `somename.sauteed.onion`?

That maybe pushes the problem further to have an additional system, such as a
blockchain-based registry, so this application is still a rough idea.

Also, such additional `.sauteed.onion` domains may be uneeded since Sauteed
Onions already handles the issue of having short memorable names (via DV certs).

This idea is more like having aliases in the Tor Browser and can be considered
a low priority application to discuss, as sauteed onions certificate already
solves most of the problem this idea tries to tackle.

Another simpler approach woud be more UX-oriented in the Tor Browser when
accessing a site with a sauteed onion certificate through it's Onion Service,
with a behavior similar to the [Alt-Svc method][]:

1. In the address bar it's shown the DNS name (and not the .onion address) but
   the connection is done via Onion Service.
2. The Onion Service connection icon is shown in the address bar (the same that already
   happens when visiting Onion Service sites).

[Onion Name]: https://securedrop.org/news/introducing-onion-names-securedrop/
[Alt-Svc method]: https://blog.cloudflare.com/cloudflare-onion-service/
