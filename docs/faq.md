# FAQ

Some guiding questions and possible answers about the pratical implementation of
[Sauteed Onions](https://www.sauteed-onions.org).

## What is the introductory reading list on the subject?

1. [RFC 6962 - Certificate Transparency](https://datatracker.ietf.org/doc/html/rfc6962).
2. [Certificate Transparency - ACM Queue](https://queue.acm.org/detail.cfm?id=2668154).
3. [Certificate Transparency : Certificate Transparency](https://certificate.transparency.dev/) site.
4. [Sauteed Onions draft paper](https://www.sauteed-onions.org/doc/paper.pdf).
5. [draft-dkg-intarea-dangerous-labels-01 - Dangerous Labels in DNS and E-mail](https://datatracker.ietf.org/doc/draft-dkg-intarea-dangerous-labels/)
6. [Attacks on Onion Discovery and Remedies via Self-Authenticating Traditional Addresses](https://arxiv.org/pdf/2110.03168.pdf)

## How it works, conceptually?

* Creating a cert for an existing `anythingherewhateveriwant.example.org` is like
  a way to append in the CT logs a message sent by whoever controls example.org.
* The sauteed onion convention would then be a way to declare in both the
  certificate (and hence on the CT Logs) that example.org controls some onion
  address (`pubkeyonion.example.org`, where `pubkey` is something like
  `test35n4rit2dzagyzixi7kfktuzns3q464donfggtn5jhflqvwihrqd`).
* Sauteed onions can also be understood as a glue between keys (the TLS
  certificate and .onion keys), so it may have additional applications we're
  still not aware of.
* You can specify anyone's onion address in a example.org subdomain, much like
  you could instruct a web server to set an arbitrary Onion-Location HTTP
  header.
* The only difference here is that the association happens in a transparent
  certificate rather than after establishing an HTTPS connection and reading an
  HTTP header.

## What is the cost of running it?

Cost of running: there are (three, four?) parts to this:

1. Constant. download all CT logs (some work to setup but quite modest and
   predictable to run).
2. Store all sauteed onion certs (a drop in the ocean of all certs, cheap).
3. Serve the api (cost depends on what type of loads should be supported).
4. Ensuring that the above is up-and-running (potentially costly with an SLA,
   unless the operator is already staffed to satisify a given sla due to
   operating other services).

## Who wold run it?

Probably someone that already hosts similar stuff (like a log, monitor, etc.).

## Sauteed Onions is compatible with Onion Services' subdomains?

As of mid-2022, there aren't many docs on the subdomain support for .onion addresses
(see [this note for details](https://gitlab.torproject.org/tpo/web/community/-/issues/270#note_2799525)),
but it's a supported feature of the technology.

That said, consider the following case:

* DNS domain: `subdomain.example.org`.
* Corresponding Onion Service:
  `subdomain.test35n4rit2dzagyzixi7kfktuzns3q464donfggtn5jhflqvwihrqd.onion`.

How a Sauteed Onion SAN would look like for such case?

We have some options:

1. `subdomain.test35n4rit2dzagyzixi7kfktuzns3q464donfggtn5jhflqvwihrqdonion.subdomain.example.org`.
2. `test35n4rit2dzagyzixi7kfktuzns3q464donfggtn5jhflqvwihrqdonion.subdomain.example.org`,
    but then the client would need an unambiguos logic to determine if
    `subdomain` should be appended to to the .onion address.

## Who would run the resolver API?

That's a good question... which would depend on the needed resources to
run an instance and the trust level required (ideally none?).

Part of the trust seems to be already solved in the client side (say Tor
Browser), since it should already be able to validate certificates.

## Which services the web browsers use? Can we use the same with a middleware proxy API?

* To be discussed.

## In the future, could we have a _sauteed onion_ certificate which also works for accessing the Onion Service using HTTPS?

Can we consider a future certificate for `example.org` having also the following SANs?:

* `test35n4rit2dzagyzixi7kfktuzns3q464donfggtn5jhflqvwihrqdonion.example.org`.
* `test35n4rit2dzagyzixi7kfktuzns3q464donfggtn5jhflqvwihrqd.onion`.

This would make the same certificate available for both discovery of the .onion
address for `example.org` and also to stablish a HTTPS connection to it
(`https://test35n4rit2dzagyzixi7kfktuzns3q464donfggtn5jhflqvwihrqd.onion`).

## Could Sauteed Onions also benefit from CAA/DANE records?

CAA records are defined at [RFC 6844](https://datatracker.ietf.org/doc/html/rfc6844).

The API/monitor/resolver could do an additional check if the CA signing the
certificate matches those defined in a `CAA` DNS record, as an additional
protection.

Or does it is already covered by CT Logs (or made irrelevant by it)?

From [Certificate Transparency - ACM Queue](https://queue.acm.org/detail.cfm?id=2668154):

> Another alternative is to base trust in DNSSEC (Domain Name System Security
> Extensions). Two mechanisms exist for this purpose: DANE (DNS-based
> Authentication of Named Entities; https://tools.ietf.org/html/rfc6698) and
> CAA (Certification Authority Authorization;
> https://tools.ietf.org/html/rfc6844). Strictly speaking, CAA makes DNSSEC
> optional but strongly recommended. Both DANE and CAA associate particular
> certificates or CAs with hosts in the obvious way by having appropriate DNS
> records for the host's name. The difference is the way the records are used:
> DANE records are to be checked by clients when connecting to servers; CAA
> records are to be checked by CAs when issuing certificates. If the CA is not
> included in the CAA record for the new certificate, then it should refuse to
> issue.
>
> Both proposals have problems that are inherent in DNSSEC, but CAA also has
> the further issue of not really solving part of the problem—namely,
> mis-issuances by subverted or malicious CAs. Clearly they will simply not
> bother to consult the CAA record.
>
> More importantly, however, DNSSEC, like the existing PKI (public-key
> infrastructure), introduces trusted third parties into the equation, which
> may or may not fulfill their duties. The trusted third parties in this case
> are DNS registries and registrars. Sadly, their security record is
> substantially less impressive than that of CAs.

## How long it takes to a new sauteed onions certificate to show up in APIs?

That's something important to inform site administrators: creating a sauteed
onion certificate can take minutes, but having it available in APIs might take
some time.

New certificates are available on CT Logs typically seconds to minutes, but
to appear in the APIs will really depend on the implementation.

A good recommendation for a sauteed onion search API would be to document
their estimated propagation delay, e.g,, by making a note of when a
certificate is observed and available for search. This timestamp can be
compared to the log entry's timestamp (i.e., when a log issued an SCT).

## Is it possible to get the whole certificate from CT Logs?

Apparently yes.
From [RFC 6962 - Certificate Transparency](https://datatracker.ietf.org/doc/html/rfc6962):

> If a certificate is accepted and an SCT issued, the
> accepting log MUST store the entire chain used for verification,
> including the certificate or Precertificate itself and including the
> root certificate used to verify the chain (even if it was omitted
> from the submission), and MUST present this chain for auditing upon
> request.  This chain is required to prevent a CA from avoiding blame
> by logging a partial or empty chain.

But seems like there's no API method to get a certificate given a domain name.
Fetching of the CT Logs is needed to build a separate database indexed by
domains.

## Is DNSSEC an option? E.g., put `_onion.www.example.com` in a TXT record?

* "TLSA but for onion addresses".
* Pro: don't need to involve X.509v3 certificates at all.
* Pro: existing machinery, no-one needs to host a new search API (i.e., use
  DNS).
* Con: site owners need to configure DNSSEC to be part of this (i.e., it is
  probably easier to update an already existing HTTPS certificate setup).
* Con: the DNSSEC hierarchy is in a priveleged position like CAs, but without
  transparency like CT (could be fixed, but a large project on its own).  We
  did not arrive at a conclusion if we would get or need [property (b)][] here.

[property (b)]: /applications/#zoomed-out-discoverability-goal

## How such a system could look like, scale and cost?

Questions from the [introductory presentation](/slides/presentation):

1. How such a system could scale if querying happens
   every time an user asks for a site and if the request goes through an API
   served via a set of Onion Service "resolvers", each one configured using
   something like Onionbalance for additional performance and failover?

2. How much such infrastructure would cost in terms of resources
   and maintenance?

3. How that backend architecture would look like?

These questions are still under discussion.
Check the [demo page](/demo) for details.

One concern is that, to be worthy and ecological, the CT Log monitoring and API
querying needs to be done in a networked, decentralized/distributed fashion.

## Does Sauteed Onions protects from certificate forgery?

A forged, but "valid" cert (say from a compromissed CA) can contain SANs
related to ilegitimate `.onion` addresses, and could be used along DNS
hijacking to redirect users to rogue sites.

The Onion-Location HTTP header is also vulnerable to this attack.

But that's exactly the purpose of CT logs, and a reason to:

1. Actively monitor CT logs.
2. Web browsers reject certs without valid SCTs.

For an even stronger fix, bi-directional associations could be implemented, see
the discussion in the [applications page](/applications).
