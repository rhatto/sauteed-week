# Discussions

Discussions to be edited and organized.

## The Sauteed Onion X509v3 Extension

```
10:06  <rhatto> i'm curious about the x509v3: i guess it would consist of some kind of record with the onion address, and the certification emission would include a step similar to what HARICA uses to emit certificates for .onions
                (generating a CSR from a nounce)
10:07  <rhatto> is something like that? i'd like to include a short summary about it in the docs, perhaps at the "Applications/Requirements/The sauteed onion criteria" section
10:08  <rgdd> something like that yes; to get the property of a unidirectional association it just needs to be there though, which is different compared to a .onion cert with harica
10:09  <rgdd> encoding the exact same information that we're doing now, just in a different place in the certificate basically (and still with the criteria that it would be invalid if example.com is not domain validated)
10:10  <rgdd> if you were to have that together with a .onion address, it would be an explicit mutual association (i.e., bi-directional)
10:11  <rgdd> (this is why it is so easy to make working now by adding an additional SAN; the same behavior can be modelled.)
10:12  <rhatto> bi-directionally would then be a plus over the current setup, and built in the extension... interesting!
10:12  <rgdd> can add another interesting future thing to think about
10:13  <rgdd> how many sites are there would onion location right now?
10:13  <rgdd> and how is that changing over time?
10:13  <rgdd> it says something about an approach like
10:13  <rhatto> wow, good question!
10:13  <rgdd> "just deliver the entire sauteed onion data set to the browser, similar to securedrop names"
10:13  <rgdd> the fun part is that its a good question independent of sauteed onions though :)
10:14  <rgdd> and its relatively straight forward to answer
10:14  <rgdd> since we have the domains to visit via ct logs :)
10:14  <rgdd> (i am obv gonna mention this in the future work section of the refactored paper btw :p)
10:14  <rgdd> probably the most interesting parameter for any potential list-based approaches
10:16  <rgdd> (upper bound i should say; the sauteed onion data set is currently like what; 4? :D)
```

## Sauteed Onion Search Engine under censorship

```
07:35  <rgdd> i think the search engine angle falls apart a bit if you drop the strong censorship threat model
07:36  <rgdd> since you could just connect to the site and do discovery that way then (spinning of your docs rhatto)
07:36  <rgdd> a potential loss is that there has to be a connection to the regular site **and** the onion site tho
07:38  <rgdd> which is likely helpful for traffic analysis (you are touching the same thing in two places, regular site and onion site)
07:38  <rgdd> wrt. doing cert-based or header-based onion location (regardless of if we want some kind of verifiable search engine or not)
07:39  <rgdd> a certificate already provides machine for a TLS setting that is "not web" (i.e., with no HTTP headers)
07:39  <rgdd> the possibility of a site redirecting to many onion addresses could become visible (e.g., in an attempt to partition users)
07:39  <rgdd> it would also make it easy to detect if other sites redirect to your onion service (i guess that is useful information?)
07:40  <rgdd> i'm intentionally **excluding any potential extras** here to help us understand which things achieve what for whom
07:41  <rgdd> (i feel like that is the problem with something that is very entagled and potentially relating to many different things in subtle ways)
07:43  <rgdd> s/machine/machinery/
07:44  <rgdd> (and can easily lead one astray to not then look at each subtle problem individually instead)
```

## Unsorted logs from 2022-06-29

```
22:31  <dkg> i wonder whether anyone has thoughts about how it relates to the concerns outlined in https://datatracker.ietf.org/doc/draft-dkg-intarea-dangerous-labels/
06:28  <rgdd> morning, and hi dkg!
06:28  <rgdd> i agree that there is "almost always a better protocol design choise"
06:28  <rgdd> i'm gonna try to avoid writing a wall about other design ideas and instead address your question :p
06:29  <rgdd> the dangerous-label take was not on my radar until now, much appreaciated that you pointed it out!
06:29  <rgdd> so far we have been thinking that sauteed onions is completely "opt-in"
06:29  <rgdd> this is evidently not the case, and i think it is crucial to not encourage things that require "opt-out" in existing setups
06:31  <rgdd> do you agree that the dangerous-label angle would be mitigated if sauteed onions was redefined as follows: (1) certificate must contain <addr>onion.example.com AND (2) certificate must contain example.com ?
06:31  <rgdd> i.e., it is not sufficient to just have (1) to conclude something about example.com's onion address

10:42  <rgdd> rhatto: could you explain what you mean in that paragraph?
10:43  <rgdd> i'd say what you describe there is what sauteed onions provide via ct, e.g., you could have a webext that on seeing example.com.sauteed.onion does a look-up based on the sauteed onion search API
10:44  <rgdd> (for example.com)
10:46  <rgdd> btw if you'd like to scim one more thing, perhaps https://www.usenix.org/system/files/conference/usenixsecurity18/sec18-winter.pdf would be useful as well
10:46  <rhatto> rgdd, yes, in that case the onion name would be redundant... what i meant is:
10:47  <rhatto> suppose there is subdomain.example.org and subdomain.anotherexample.org: how both could be represented in as a <something>.sauteed.onion domain?
10:49  <rhatto> (if that makes any sense to have a .sauteed.onion Onion Name allocation if the TLS certificate already has the DNS addr -> onion addr association)
10:51  <rhatto> by onion name i mean a scheme like this: https://securedrop.org/news/introducing-onion-names-securedrop/
10:52  <rhatto> (which is being integrated on TBB core with support for multiple rulesets: https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40458)
10:53  <rgdd> so you want to merge two different names A and B into another name C.sauteed.onion?
10:54  <rhatto> rgdd, no... in that case we would have some conflict: who gets the subdomain.sauteed.onion?
10:54  <rgdd> yeah that's why i'm asking, sorry i'm still confused!
10:55  <rhatto> i see it's an idea full of ambiguities... and maybe useless if one is already using sauteed onion certificates
10:55  <rhatto> rgdd, i'm also confused by this idea! might as well just erase it from the applications page if we find it pointless hehe :)
10:56  <rgdd> just some dots are missing for me so hard to say if it makes sense or not :p
10:56  <rgdd> i do like the sauteed.onion convetion to make the search native tho!
10:57  <dkg> i'm trying to understand the tradeoffs myself here -- it's hard to have this conversation when i'm not clear on what's being used as an example and what's an actual literal string
10:57  <dkg> for example, is the sauteed.onion DNS suffix literal?  who is allocating this "sauteed" subdomain of .onion?
11:00  <dkg> rgdd: i'm intrigued by the idea of only accepting this label if it's got *both* names in the SAN field in a single certificate, but i'm also unsure about the broader implications of that.  for example, are we saying that a
             <foo>onion.example.com dns label MUST be rejected by any X.509 certificate validator if it is seen *without* the example.com SAN in the same cert?
11:00  <dkg> that seems like it would require an overhaul of every X.509 cert validator to be safe, which seems… i guess "implausible" or something
11:02  <dkg> but instead i think you might just be saying that the tor-aware toolkit shouldn't be willing to try <foo>.onion as an "alt-service" based on X.509 certificate alone, *unless* that SAN couples it with a given cert.
11:04  <rgdd> dkg: okay cool, yeah my intention was the latter; regular Firefox would just visit the site and see great, a valid cert for example.com; whereas TB might know that if there is also <addr>onion.example.com that is a
              declaration of an associated onion address
11:04  <rgdd> and similarly any monitor that exposes a search API based on what appear in the logs would also follow that verification logic; must have both SANs
11:05  <rgdd> (and clients using answers from such an API would of course also require both SANs)
11:05  <rgdd> (example of an search api: https://gitlab.torproject.org/rgdd/sauteed-onions/-/blob/main/monitor/doc/api.md)
11:06  <rgdd> re: sauteed.onion; the idea there was that it would be a convention in tor browser similar to tor.onion in SecureDrop, right rhatto?
11:06  <rgdd> so that if a user types example.com.sauteed.onion in the URL bar and clicks enter, what that really does is look-up onion addresses for example.com that were found in CT logs
11:12  <dkg> i'm also curious about whether you've talked with the folks from digicert, or any other CA that is already issuing certs for *.onion
11:13  <dkg> (that is, not a literal wildcard *.onion, but certs for specific onion addresses)
11:14  <rgdd> we havn't, still very early days
11:14  <rgdd> note that that certificate issuance is orthogonal though
11:14  <rhatto> rgdd, right; and thank you both for parsing this idea; it's more like having aliases in the tor browser; but i would say it's a low priority idea to discuss right now
11:14  <rgdd> sauteed onions help users discover onion services
11:15  <rgdd> .onion certificates as issued by harrica and digicert make onion services function with https
11:15  <rgdd> but a prerequisite to find an onion site with https is to know its address, which is what sauteed onions help with
11:25  <dkg> so i think i understand the general proposal here, but i'm still trying to wrap my head around how we frame the value proposition.
11:25  <dkg> is the added value the free certification by CAs that are unaware that they're certifying a .onion address?
11:26  <dkg> or is it discoverability by clients via CA monitors/logs?
11:26  <dkg> or some other advantage?  or several different things?
11:28  <dkg> if it's the free certification, i can imagine some CAs being a little bit pissy that they're being asked to attest to something that they have no understanding of or intent to assert.  might be worth talking to some CAs that
             *don't* currently issue .onion addresses as well as those that do, to see what their perspective is on this.
11:29  <dkg> if it's discoverability, it seems like there could be several simpler forms of discoverability, which could have less problematic data trails than talking to the CT logs (or their monitors)
11:30  <dkg> but i might be wrong in these evaluations -- and i'm sure y'all have thought about them more than me.  if you see things i'm missing, i'd be happy to learn.
11:30  <dkg> meanwhile i'll go back and try to read all the docs.
12:46  <rgdd> dkg: a value proposition is that it should be detectable if users get the wrong and/or targeted onion address for a domain name
12:47  <rgdd> so having a CA perform name validation and holding it accountable via a transparency log seems like a step in that direction
12:47  <rgdd> another value proposition is that TLS sites can be censored so that onion location does not work
12:47  <rgdd> the ability to fall back on a public log is then quite appealing, it is hard to censor selectively and it is mirrored
12:48  <rgdd> yet another value proposition is that now .onion certs **are part of the CA/CT log ecosystem**, but users can't easily determine who runs those sites
12:48  <rgdd> i think it is a mistake to frame the value with regards to "free certs that work now" (it will be cut from the next paper itteration)
12:49  <rgdd> the correct framing is probably more like "here's an incremental deployment idea"
12:49  <rgdd> on paper my preference would be to have "criteria (1)" in an x509v3 extension
12:49  <rgdd> the obvious problem is that the journey to get there is likely long, if it happens at all
12:50  <rgdd> a fast path that is fully compliant would be to issue a certificate with an actual .onion address and www.example.com
12:50  <rgdd> (which was the initial seed that made us start looking into sauteed onions - what can we do with that?)
12:50  <rgdd> after some more thought i think that would be worse though due to connection coalescence
12:51  <rgdd> unlike other piggy-back proposals, i do think the current implementation idea is quite close to a legitimate use-case
12:51  <rgdd> e.g., Mozilla's BT (as a contrast) had no interest in domain validation; just making a signed checksum public
12:52  <rgdd> and if that caught on, CT logs would be full of dummy certs that don't even need all the complexities of CT..
12:52  <rgdd> fwiw a somewhat similar "subdomain hack" did not receive any complaints from CT at least: https://words.filippo.io/how-plex-is-doing-https-for-all-its-users/
12:52  <rgdd> what was a collab with digicert, any you're right that speaking to LE before going all out would be a good idea
12:53  <rgdd> making our case on the ct policy list is probably yet another milestone that should be checked-off
12:53  <rgdd> i think it is too soon to do this type of outreach though
12:54  <rgdd> dkg: i'm curious what you had in mind for simpler forms of discoverability
12:54  <rgdd> discoverability is a large part of the framing here
12:55  <rgdd> there might be more, but that one i think is the easiest to argue for
13:05  <dkg> hm, i think the plex situation is pretty different -- plex explicitly negotiated with a CA to get a custom intermediate authority allocated to them.
13:05  <dkg> that kind of direct negotiation with a CA is out of reach of most onion service operators
13:06  <dkg> *and* it's scoped entirely within the plex.direct DNS zone, not applied globally with any suffix the way the sauteed onions proposal is
13:07  <dkg> re: discoverability: querying a CT log for a given name isn't cheap, and it isn't particularly easy to do from the CT protocol, if i'm remembering how it works correctly.
13:11  <rgdd> from a log operator perspective plex is not different though; and that's where things have been tighting a bit lately (like:
              https://docs.google.com/document/d/18hRJQW5Qzgcb87P-YIkWIuna9_pRUNGsZPC4V0fiq5w/edit#heading=h.sbtuh0lu8kj2)
13:12  <dkg> my recollection is that CT relies on monitors that review and audit the logs to be able to catch certificates for any given name -- the CT log itself lets you prove inclusion of any given cert
13:12  <rgdd> yeah so to facilitate the query you need someone that exposes a search api
13:12  <rgdd> similar to crt.sh
13:12  <dkg> right, and now you leak your queries to them
13:13  <dkg> (and you introduce a dependency on a non-standard lookup protocol)
13:13  <rgdd> that's not worse than searching on duckduckgo, who doesn't even have a good chance to provide an accurate mapping at scale
13:13  <dkg> (which makes the system even more vulnerable if the designated monitor (the thing that exposes the search api) changes policy or goes rogue)
13:14  <dkg> i agree this is better than searching on ddg!
13:15  <rgdd> and while searching on crt.sh would be a non-standard thing, just writing down an api that can be used for search would solve that (since this is something users want:
              https://www.usenix.org/system/files/conference/usenixsecurity18/sec18-winter.pdf)
13:16  <rgdd> you can also make such an API untrusted
13:16  <rgdd> again, unlike DDG
13:16  <dkg> how do you mean "untrusted"?
13:16  <rgdd> the API can for example not fabricate example.com -> onion addr easily
13:16  <dkg> what confirmation do you have that the API won't withhold data about any particular query?
13:17  <rgdd> that is currently out-of-scope in the API we linked above, but it can be extended to solve that as follows:
13:17  <dkg> you're saying the monitor can't produce false *positive* results, because you'll verify the X.509 certificate against some root CAs
13:17  <rgdd> that's not what i'm saying
13:17  <rgdd> you're correct
13:17  <dkg> i'm saying the monitor can *withhold* true positive results, yielding a false *negative* result.
13:17  <rgdd> and you can add more on-top here
13:17  <rgdd> to solve that issue
13:18  <rgdd> so you now have basically a sauteed onion data sets of certificates from ct logs right
13:18  <rgdd> you can put this into an ordered merkle tree that allows efficient (non-)membership proofs, like a sparse merkle tree
13:18  <dkg> abstractly, yes.  it's not clear to me yet that the client has reliable access to them.
13:18  <rgdd> (you might remember "log-backed map")
13:19  <dkg> who crafts and maintains this ordered merkle tree?
13:19  <rgdd> the api
13:19  <rgdd> (which signs the tree head periodically)
13:19  <dkg> ??  an api isn't an actor, it's a description of an interface, right?
13:20  <rgdd> the search api is operated by a party
13:20  <rgdd> much like a log and a monitor
13:20  <dkg> (i also need to 'fess up that i don't know off the top of my head how a merkle tree allows efficient non-membership proofs, but i'm happy to take your word for it for now)
13:21  <dkg> so this is like a next-gen monitor that itself commits to some amount of transparency, right?
13:21  <rgdd> yes, exactly
13:21  <rgdd> (and also one moment, will give you two links to queue up for reading if you'd like.)
13:22  <dkg> sure, but i won't read them now ;)
13:22  <rgdd> https://github.com/google/trillian/blob/master/docs/papers/VerifiableDataStructures.pdf
13:22  <rgdd> https://eprint.iacr.org/2016/683.pdf
13:23  <rgdd> yeah, no need, just in case you are interested at some point
13:23  <dkg> the trillian paper doesn't appear to allow efficient non-membership proofs
13:25  <rgdd> it does, see verifiable map and verifiable log-backed map
13:25  <dkg> ah, right; sorry, was looking at the summary of the verifiable log.
13:26  <rgdd> so the picture that you should paint for yourself is that you can represent a log as a verifiable dictionary
13:26  <rgdd> and in our case we only include a small subset of certs into that dictionary (a deterministic selection criteria)
13:27  <rgdd> if the api does this wrong, there would be a cryptographic proof for it
13:27  <rgdd> so that's what i meant by it being untrusted; to be more correct i should say it is not trusted blindly ("trust but verify")
13:28  <dkg> right, i get what you  meant by "untrusted" in this case
13:28  <dkg> ok, but zooming out a bit: this is a lot of machinery for a lookup that you'd hope would be more straightforward.  i'm assuming that we want to avoid the client directly visiting www.example.com on port 443, right, because if
             they do that they can just use some kind of alt-svc header, right?
13:29  <dkg> so the (zoomed out) discoverability goal is that the client gets to query some third-party service for some record that has the semantics "www.example.com's onion service is <foo>.onion"
13:30  <rgdd> yeah, or at least have the ability to not do that connection
13:30  <dkg> and they ought to be able to (a) cryptographically verify against some *other* authority that the binding is correct before using it, and (b) they ought to have some amount of confidence that enables them to distinguish
             between "lookup failure" and a cryptographically-strong "no such binding" response.
13:31  <rgdd> (and in some cases the connection may not even be possible, e.g., in the case of cencorship)
13:31  <dkg> is that the right scope for the discoverability goal?
13:32  <dkg> (good call about thinking clearly about the censorship case directly, agreed)
13:32  <rgdd> yeah, i think you summed it up well
13:32  <rgdd> another one that i'm less sure about
13:33  <rgdd> is whether its bad that it is easy to hand out different onion addresses to different users with onion location / alt-scv
13:33  <rgdd> that kind of behavior would also get stuck in ct logs now
13:34  <rgdd> it could potentially be bad if users bookmark stuff
13:34  <dkg> you're saying that being able to opt into using a global view is preferable (for the client) because it puts them in a larger anonymity set among the other clients of the service.   right?
13:34  <rgdd> yea
13:35  <dkg> i think that's right, but that might be a distinct goal from the discoverability goal.  falls out of the same architecture in this case, though!
13:36  <rgdd> yeah so i guess that is more detectability than discoverability
13:36  <dkg> so i can't help but think that this smells like a DNS lookup, with cryptographic integrity/authenticity backed by DNSSEC.
13:37  <dkg> is DNSSEC itself off the table?  (as in, we don't want to require a service operator to enable DNSSEC for their zone)?
13:38  <rgdd> i wouldn't say it is off-the-table
13:38  <dkg> dumbest possible counterproposal:   a TXT record for _onion.www.example.com with contents <foo>
13:39  <dkg> i'm not saying i prefer this, i'm just saying that the machinery for it is probably already much more readily available.
13:39  <dkg> you've got dozens of confidential proxies/caches that you can use; an already standardized protocol to piggy-back off of, etc.
13:40  <dkg> it even gives you some of the global visibility aspects
13:40  <dkg> help me understand what the current proposal has that's preferable to this
13:40  <rgdd> well
13:40  <dkg> (or what the major drawbacks are to a TXT for _onion -- i'm sure there are some)
13:40  <rgdd> one thing you lose is the transaprency log, there's no dnssec-transaprency for issues with that hierarchy
13:41  <rgdd> (rip: the dnssec-transaprency stuff ln5 was poc:ing long time ago :p)
13:41  <dkg> dnssec-transparency has been discussed in the past, but i agree that no one has implemented it.
13:41  <dkg> that's a bummer
13:41  <dkg> but i'm not sure how much of an impact it has on the discoverability question
13:42  <rgdd> it doesn't impact the discovery itself, but the integrity of the discovery
13:42  <rgdd> i mean we can make an even dumber proposal
13:42  <rgdd> no dnssec
13:42  <rgdd> dig a txt record
13:42  <dkg> one obvious distinction is that TXT-for-_onion requires the site operator to opt into DNSSEC, right
13:43  <dkg> do you really think that looking up a TXT record and validating its DNSSEC signature is *more* complex for the client than the current proposal?
13:43  <rgdd> no, but i think it is more complex for the site operator to work with dnssec
13:44  <dkg> if we say "no dnssec" then we lose the cryptographic confidence from discoverability subgoals (a) and (b)
13:44  <rgdd> yeah, but we also don't have the same amount of confidence with just dnssec as a proposal with a transparency log
13:45  <rgdd> without*
13:48  <rgdd> i don't disagree though, dnssec would be an alternative path to try
13:51  <rgdd> and it is of course appealing to not have to host an api
13:52  <dkg> what confidence do you lack with DNSSEC that you would have with a transparency log?
13:54  <dkg> with DNSSEC, it's always possible that the authoritative zone can sign multiple disjoint RRsets, and the DNS resolver that you query can select among them to give you the one of their choice.
13:54  <dkg> without proof of non-membership or proof-of-exhaustive-response, the CT monitor that you're querying can do the same thing
13:56  <rgdd> yeah so it is mainly at those intersections i'm missing transaprency logging; i'm not sure if there are problems with not logging the actual RRs (like _onion.www.example.com)
13:56  <rgdd> if you did log at those intersections such dis-joint stuff would be detectable
13:56  <dkg> another distinction is that the CT log itself depends on cryptographic authority of all the CAs in the ecosystem and good behavior of the CT log itself, whereas DNSSEC-backed DNS query depends on the good behavior of all the
             parent zones up to (and including) the root.
13:58  <rgdd> in my head i compare "CA good behavior required" and "parent zones good behavior required" as the comparable counterparts
13:58  <dkg> to get the actual proof of non-membership and proof-of-exhaustive response (so that the monitor can't selectively hand the client one of several valid certs, the equivalent of the multiple DNS RRsets scenario), you need an
             additional transparency infrastructure on top of that.
13:58  <rgdd> both benefit from transparency
13:59  <dkg> just to be sure we're on the same page: what do you think is the transparency benefit to the client, concretely?
14:00  <dkg> is it that the site operator can check to ensure that no one else is signing certs that contain these particular labels?
14:00  <dkg> (that's only an indirect benefit to the client, right?)
14:02  <dkg> i mean, i'm already personally convinced that some form of DNSSEC transparency is a valuable thing for the ecosystem, particularly logging and requiring corroboration for records that traverse a zone cut.
14:03  <dkg> but that seems orthogonal to the discoverability question here
14:05  <rgdd> what i do want to see is that where there are signing operations, those signing operations become transparent
14:05  <rgdd> because keys get compromised
14:06  <rgdd> protecting the zone transitions (sorry probably the wrong lingo) is a first right, if that can be abused it doesn't really matter what the real zone owner is doing
14:06  <rgdd> that would be the lower-hanging fruit dnssec stuff
14:07  <rgdd> so i don't think this is orthogonal if the goal is to have discovery for which integrity is hard to tamper with without detection
14:08  <dkg> yep, i agree about the lower-hanging fruit of dnssec transparency -- and zone administrators can always rotate their own zone signing keys if they believe they may have been compromised.
14:08  <rgdd> i'm currently in *hmmm* state for if i'd be happy with discovery after that point
14:09  <dkg> in some ways you can imagine a TXT-for-_onion as comparable to a TLSA record, right?
14:09  <rgdd> because while you could of course block an answer and the client learns "no such thing", you could always just block the connection right
14:09  <rgdd> dkg: yep!
14:09  <rgdd> that's actually a very good comparison
14:10  <dkg> TLSA of the EE cert, in particular (i can never remember the TLSA variant numbers)
14:10  <rgdd> i still think it is interesting to have transaprency for the keys signing RRs in a zone as well, but it is also not comparable to a CA signing key that is signing on behalf of others
14:11  <dkg> right -- also an entirely different project than sauteed-onions, i didn't mean to get us into a super wide digression 😛
14:11  <rgdd> (perhaps i should add that i'm very interested in making key-usage transparent, if you want to queue up another work-in-progress thing for a rainy day: www.sigsum.org)
14:11  <rgdd> haha
14:11  <rgdd> i enjoyed it, happy to talk!
14:12  <rgdd> i think if we had more time, we would have explored the dnssec route more throughly
14:13  <rgdd> and there's plenty of future work here to consider
14:13  <rgdd> i mean, could you get this type of look-ups into the DHT?
14:13  <rgdd> how would that work, etc.
14:13  <rgdd> :p
14:13  <dkg> you mean into Tor's DHT?  or some other ledger?
14:13  <rgdd> no, Tor's DHT
14:14  <dkg> right, the question then is "on whose authority?"
14:14  <dkg> i guess you could work on "DNSSEC-signed _onion TXT records will be redistributed in Tor's DHT"
14:14  <rgdd> yeah, so exactly that
14:14  <rgdd> is what we were thinking
14:14  <dkg> ("with RRSigs)
14:14  <rgdd> and said
14:14  <rgdd> cool future work
14:15  <rgdd> (i think none of us really had much time to work on sauteed onions, the bulk of what we have done was 17 intense days over last christmas :p)
14:15  <rgdd> and its been a best-effort kind of thing since then
14:15  <rgdd> trying to explore further
14:15  <dkg> understood, that's a familiar situation
14:16  <rgdd> i think it's time for me to find some dinner
14:17  <rgdd> (i'm located in sweden, ~7pm here!)
14:18  <dkg> understood, thanks for the conversation!
14:18  <rgdd> likewise, hope to talk more soon!
```

## Unsorted logs from 2022-06-30

```
09:03  <dkg> rhatto: one other takeaway from yesterday's discussion from me that didn't make it into rgdd's list is that to get property (b) from the current proposal without some additional machinery on top of CT
09:05  <dkg> in particular, the reliance on crt.sh to look up a cert *by name* means that the crt.sh operators can claim "no such certificate exists" and the client has no way to verify it
09:09  <dkg> (this is distinct from the DNSSEC situation, where the DNS resolver could return a signed NXDOMAIN response and the client *could* get the cryptographic verification of "no such record" that is described in (b) )
09:11  <dkg> furthermore, without the additional machinery on top of CT, the "Misc goal related to anonymity" fails.  That is the lookup-based proxy (either crt.sh or the DNS resolver) should be unable to assign the querying client into a
             selected anonymity set when multiple records exist.
09:12  <dkg> (the same is true for the DNSSEC alternate proposal, fwiw -- when multiple signed RRsets exist, the querying client is at the mercy of the resolver as to which one gets served)
09:17  <rgdd> dkg: thanks, added one more bullet point in the pad under sauteed onions discussion
09:18  <rgdd> dkg: regarding signed NXDOMAIN response that no answer exists, would that require that it is configured explicitly or is that a default thing if you have DNSSEC setup?
09:20  <rgdd> dkg: i'd say the "misc anonymity property" is reduced without (b), but the possibility of targeted behavior would still be visible in the log even if the client; obv not ideal but also not a complete loss
09:21  <rgdd> *even if the client only sees one answer
10:45  <dkg> rgdd: right, DNSSEC offers signed NSEC (or NSEC3 or NSEC5) records as cryptographically strong proof of non-existence
10:46  <dkg> i shouldn't have said "NXDOMAIN" since you can't sign an NXDOMAIN record (it doesn't contain the information needed to be evaluated), but the NSEC variants provide the correct semantics and are signable.
10:57  <rgdd> dkg: and NSEC (the basic version) is pretty much an (authenticated) linked list right where each RR is signed and chains to the next one?
10:58  <rgdd> at some point i should try and set this up to get a better feel for it
11:27  <dkg> rgdd: it doesn't have to be implemented that way, but that's the most efficient and "offline" way to implement it.  the ZSK signs NSEC records that cover the gaps between every other record in the zone.
11:27  <dkg> the complaint some people have about NSEC is that when you implement it that way it lets someone enumerate all the labels in your zone pretty easily
11:28  <dkg> (whether zone enumeration is actually a problem or not is a separate question -- CT itself has raised concerns that it is effectively allowing zone enumeration for all records that point to TLS services)
11:29  <dkg> NSEC3 makes zone enumeration a bit harder by hashing all the existing records and then ordering the hashes and signing the gaps between the hashes.
11:30  <dkg> this allows for a quick enumeration of hashed labels, and then an offline attack can be mounted against the hashes (see https://dnscurve.org/nsec3walker.html for djb's poc that this isn't a particularly strong defense against
             zone enumeration)
11:31  <dkg> NSEC5 is a much newer proposal that uses a VOPRF along with a special-purpose signing key that is live on the authoritative server to produce a cryptographic proof of non-existence of a single record
11:33  <dkg> NSEC and NSEC3 both also have the interesting property that they permit "negative caching" -- a DNS resolver that gathered an NSEC or NSEC3 record and caches it can return it without even querying the upstream resolver for a
             novel record that it can tell actually lands in the space covered by the NSEC or NSEC3 record.  Not every resolver does aggressive negative caching like this yet, though.
11:35  <dkg> and, of course, if you're ok with your zone signing keys being live on your authoritative resolver, you can configure the resolver to produce one-off signed, single-purpose NSEC records with the narrowest possible range that
             covers the requested record (i think this approach is called "white lies" for some reason) as a replacement for NXDOMAIN.  that approach also avoids the zone enumerability
11:35  <dkg> problem, at the cost of putting your signing keys into your authoritative server.
11:36  <dkg> anyway, the infrastructure that you'll need on top of CT to get property (b) and the global anonymity set will by default provide even more robust zone enumerability (at least for TLS-enabled hosts) than CT on its own.
11:37  <dkg> the shape of the problem space for DNSSEC and what you're trying to do is very similar :P
11:43  <rgdd> dkg: how is freshness handled with negative caching?
11:46  <rgdd> TTL?
11:47  <rgdd> or maybe there's like a validity timestamp in the signed recorded
11:47  <rgdd> (i should go and refresh the details!)
11:49  <dkg> iirc, it's based on the validity timestamp in the RRSig, not the TTL
11:49  <dkg> (since the TTL is not covered by the signature, but the validity window is)
11:49  <rgdd> (seems so yeah, was just curious how that plays together with serving stale data as in common practise - described in RFC 8767)
11:50  <rgdd> (but §10 there easily handwaved that.)
11:52  <dkg> yeah, i've always been fuzzy on the intersection between the TTL and the sig validity window in the first place -- whether for NSEC* or for other DNSSEC records, for that matter.
11:53  <rgdd> regarding the overlap between what we are looking at here concretly and dnssec, that's quite natural since dnssec could have taken on the same role as CAs
11:53  <dkg> yep, it's not surprising -- we're dealing with cryptographic authorities, names, and addresses
11:54  <rgdd> (the way i view it is that CT was an elaborate power-grab from Google, but that's a completely different rant!)
11:56  <dkg> CT addresses a real flaw in the X.509 ecosystem, which is the lemon market among CAs within the CA cartel
11:57  <dkg> even the cartel's attempt to shore up their own legitimacy with the CA/Browser forum was insufficient to demonstrate that the worst CA will adhere to their stated baseline requirements.
11:57  <dkg> CT enables *verification* that CAs are in fact adhering to the BRs by forcing all their signatures into the public view
11:57  <dkg> so yeah, it is in some sense a power-grab from Google
11:58  <rgdd> And CT also puts Google on the critical path of certificate issuance
11:58  <dkg> Google is saying "fuck you CAs we don't trust all of you"
11:58  <rgdd> exactly
11:58  <rgdd> :p
11:58  <dkg> to be fair to Google, though, they designed it all in the open and are open to other people running CT logs
11:59  <dkg> their control over the root store in Chrome already gives them the power to require arbitrary things from the CAs that they consider for inclusion
11:59  <rgdd> that's kinda a show though, it is always nice to paint something as this great ecosystem but in practise google steers this in the direction that favors them
11:59  <dkg> well, running a CT log for the entire Internet requires a ton of resources -- both technical and personnel for dealing with failure
12:00  <dkg> so anything that works at that scale is going to be expensive -- google knows that and has the resources to spare
12:00  <dkg> i know that the Tor project understands how hard it is to run systems like this that scale to the internet :P
12:00  <dkg> so yes, it's convenient for google to propose something that only someone with a lot of resources can realistically reliably implement
12:01  <dkg> they are definitely favored simply by being huge and powerful already
12:02  <dkg> but i would rather that someone huge and powerful be open about what they're doing and provide clear instructions (and software!) for other people to step up if they can pool the resources.  the other option is for them to
             just say "we will decide which CAs to include based on arbitrary, private criteria, and you will all just follow"
12:02  <dkg> that seems worse
12:04  <dkg> does Chrome accept an SCT from any non-google CT logs at this point?  or does it require an SCT from a google log?
12:04  <dkg> (i haven't kept track)
12:04  <rgdd> they just pulled that requirement because now they instead check SCTs with a trusted Google auditor with k-anonymity
12:05  <rgdd> idea being "if Google has not seen that SCT it is very suspc, please send the cert to us"
12:05  <rgdd> to be compared with "you will not get to use the certificate if we have not already observed it"
12:05  <dkg> "check SCTs"?  aren't SCTs self-checkable?
12:05  <dkg> you mean that they are auditing for misuse of the log's own signing keys?
12:05  <rgdd> not if you don't want to make a connection to the log
12:06  <rgdd> and they are not keen on ct-over-dns, prefer to query their own auditor
12:06  <dkg> i'm pretty sure they are checkable offline -- that's the whole point of an SCT
12:06  <rgdd> oh ok
12:06  <rgdd> check sct has two meanings
12:06  <_syverson> I see much of what we're doing with sauteed onions and SATAs as taking away the ability of authorities to usurp control over your domain.
12:06  <rgdd> 1) verify sig
12:06  <rgdd> 2) check inclusion
12:06  <dkg> right, i'm talking about (1)
12:06  <rgdd> the requirement right now in chrome is basically two SCTs from independent orgs
12:06  <dkg> i see what you mean about (2)
12:06  <rgdd> used to be google + not-google
12:06  <_syverson> That includes authentication, but also revocation.
12:07  <dkg> _syverson: what is SATAs?  (sorry if this should be obvious)
12:08  <_syverson> At the same time, contra DNSSEC to a fair extent, being entirely backwards compatible with existing infrastructure.
12:08  <_syverson> Sorry, sauteed onion work started as SATA-lite (less ambitious to roll out than SATAs but providing less).
12:09  <_syverson> SATAs are Self-Authenticating Traditional Addresses.
12:10  <dkg> interesting -- i only knew the hard drive connectivty standard.  i don't really see how "self-authenticating" and "traditional" can go together, a la zooko's triangle.
12:10  <_syverson> You get most of the sauteed onion elements (though it is only in the sauteed onion work that we get the transparent discovery from CT logs).
12:11  <dkg> fwiw, i don't think you get the properties you're looking for from the CT logs "with existing infrastructure"
12:11  <_syverson> But you can get self-authentication in Firefox without needing to route through Tor.
12:11  <_syverson> And of course without the protection of routing through Tor.
12:11  <dkg> rgdd and i had this discussion yesterday -- the interface you're querying provides quite a bit of leeway for crt.sh to abuse the client's trust.
12:12  <_syverson> This is definitely a conscious attempt to cope with Zooko's Triangle (again in a fully backwards compatible way).
12:12  <dkg> if anything, i'd say that the DNSSEC approach is the one that is more compatible with existing infrastructure, because it doesn't depend on a novel append-only data structure that allows proof-of-nonexistence.
12:13  <_syverson> Ahh, well the follow on proposal that I'm working on submitting today has you downloading the DB locally and making discovery there: no crt.sh.
12:14  <_syverson> What I meant about DNSSEC is that when you run it, you lose a nontrivial chunk of people because they have old incompatible stuff.
12:16  <_syverson> I have been wondering for some time about the problem that access/censorship-resistance for many of the people who need it can't assume that they have recent, quality hardware and software.
12:17  <_syverson> Not to mention the tension with degrowth and burning the planet and all.
12:17  <dkg> "downloading the db" ?  which db?
12:17  <_syverson> But ermm, maybe I'm straying from sauteed onions just a bit. ;-)
12:18  <dkg> turns out it's all connected :P
12:19  <dkg> (also, if you're talking about a DB at the scale of the internet, i'm pretty sure that loses a nontrivial chunk of people too.  we're way past the point of shared /etc/hosts files being feasible)
12:20  <_syverson> If you have a DB of the domains that have sauteed onion certs scraped from as many indep CT logs as possible and stick it on an onion service, then it can be queried but also downloaded and used a la an HTTPS-Everywhere
                   ruleset.
12:21  <_syverson> Doesn't scale for, e.g., low-resource clients perhaps once sauteed onions really take off.
12:21  <_syverson> But we should have such problems.
12:23  <_syverson> This is a proposal to have something working and available now (e.g. within months) that works for a few years. Can modify it with growth.
12:24  <_syverson> Once upon a time to run a Tor relay, you had to have met Roger personally so he would put you on his list. We've moved on a bit since then.
12:25  <_syverson> I should note that it also works for even smaller values of now (ha ha, cf. longnow.org) if you set up a sauteed onion cert for your site.
12:27  <_syverson> Because on a scale of hours or maybe a day you have blocking resistant discovery of the onion address (as long as CT logs and things like crt.sh aren't blocked) in ways that Onion Location totally fails for.
12:28  <_syverson> Sorry, short window to look at this before turning back to my proposal so you're getting a bit of a phrenetic brain dump.
12:28  <dkg> i appreciate it!
12:28  <dkg> the trick is figuring out how you trust that this DB is complete
12:29  <dkg> assuming that the internal records have cryptographic signatures (so you can confirm that each record is on its own *correct*) then you have the question of how you can be attacked by deliberate omission by the db-asembler
12:29  <dkg> these are all good steps, fwiw!  i'm not saying they're inherently bad, i'm just observing the pieces that are missing.
12:30  <dkg> and yes×100 for "we should have such problems." ☺
12:34  <rgdd> dkg: we are also super keen on critical feedback, that's what makes or breaks proposals so that we arrive at something that is actually the most appropriate solution :)
12:37  <_syverson> Yes. you can do nice things like rgdd has addressed in Verifiable light-weight monitoring for Certificate Transparency logs and we did in Privacy-Preserving & Incrementally-Deployable Support for Certificate
                   Transparency in Tor, but that's more ambitious.
12:40  <_syverson> BTW if you want to know about SATAs, check out https://arxiv.org/abs/2110.03168
12:40  <_syverson> i.e. Attacks on Onion Discovery and Remedies via Self-Authenticating Traditional Addresses
12:48  <dkg> interesting, thanks.   i haven't read the whole thing, but i'm trying to get my head around the specific example posed.  is there a better place to talk about it?  from my skim it looks like you're investing the
             query-parameter label "onion" with special powers
12:49  <dkg> if that's right, it's raising a whole new category i need to include in my dangerous-labels draft :/
12:51  <_syverson> Well it doesn't have those powers just by itself. The WebExt should cause a lookup and routing to the onion service, but it won't authenticate that connection unless the signing using the onion key and the TLS key all
                   point at each other in a correct and timely way.
12:53  <dkg> right, i'm asking how you confirm that the TLS key points to the onion key correctly
12:54  <_syverson> Plus, if you've got contextual trust restrictions only trusting certain sattestors, then you won't accept, e.g., a connection to live.com unless it has a sattestation from a Microsoft SATA you trust for this. Or won't
                   autneticate a connection to, e.g., nrl.navy.mil without a sattestation from a trusted sattestor for .mil domains.
12:54  <dkg> (in the threat i'm imagining, someone able to control some responses on a webserver wants to pretend that a particular onion service is that webserver)
12:55  <dkg> where is the set of trusted sattestors derived from?
12:55  <dkg> (along with the mapping from trusted sattestors to specific domains covered)
12:55  <dkg> (i worry this is getting super OT for this channel, happy to take the conversation elsewhere)
12:56  <_syverson> There is a SyverSAN (to use rgdd's term) in the TLS cert and the self-sattestation header is signed by the onion key and includes a fingerprint of the TLS cert. Plus some other stuff.
12:57  <dkg> ah, the SyverSAN is the thing that the sauteed-onions folks are talking about using too i guess
12:57  <_syverson> There is no single set of trusted sattestors, though it would make sense for there to be a generic one. The more generic the less contextual the trust.
12:59  <rgdd> (dkg: yeah what Paul means by SyverSAN is SAN: <addr>onion.example.com, and as we established yesterday sauteed onions would also require that example.com is DV-validated to mitigate a dangerous label in this particular
              circumstance.)
12:59  <_syverson> Yes. I think rgdd called it that because I wrote about it in a paper in 02017. Then we used it in SATAs. Then we realized we could get more immediate benefits if we had less ambitious goals as we do in sauteed onions
                   work, which again then adds the CT-log based discovery and transparency.
13:02  <_syverson> Yes. It's important that one shows control of both domains in <addr>onion.example.com, the full subdomain and example.com, but that should come for free if you are getting a cert with example.com as the subject and
                   giving the full subdomain as a SAN.
13:42  <rhatto> ops! https://crt.sh/?d=9999999999999999999999999
14:20  <rhatto> test API is online: http://sautee7hci55oro5abb27q3khgw77gyq4blt4ihhdssyjazg3klufxqd.onion/docs
14:20  <rhatto> docs online: http://sauteef2orqgllvzy3cuspmaficgttf47rpbk24j555qcrhjc4duixid.onion/
14:20  <rhatto> example usage: http://sautee7hci55oro5abb27q3khgw77gyq4blt4ihhdssyjazg3klufxqd.onion/search?in=www.sauteed-onions.org
14:31  <rgdd> rhatto: neat!! i will read everything you put down in text tomorrow morning
14:32  <rgdd> just tried some queries towards the api, seems to be working! (additionally tried www.rgdd.se and sysrqb.xyz)
14:32  <rgdd> nice work while the rest of us sit here writing walls ;>
16:36  <rhatto> rgdd, yes, it works but it's fragile (depends on the current HTML structure of crt.sh :/)... but it should be enough to present tomorrow at the demo session
16:37  <rhatto> for a (hopefully) near future, it should be possible to plug the monitor code into some database and then change the API backend to consume it
16:39  <rhatto> (and perhaps the monitor could be refactored to do fetches in parallel and be installed in some virtual machine, to have a development/test API available)
16:40  <rhatto> i'd like to sketch the architecture for a proposed next-step API service in this task: https://gitlab.torproject.org/rhatto/sauteed-week/-/issues/4
16:41  <rhatto> i should get there after playing a bit with the webext code
16:44  <rhatto> ref. writing walls: i'm really enjoying the discussion and i think it's also an important activity to have... maybe it's the most important thing happening in this hackweek project! :)
17:03  <rhatto> rgdd, i merged your summary at https://gitlab.torproject.org/rhatto/sauteed-week/-/commit/90b48d90474a42bc08881d1947e121628093c61b (split into the Applications and FAQ pages, but maybe i should kept in a single page due to
                the references... i'll see how to fix this)
17:07  <rhatto> ok, fixed the doc reference
17:08  <rhatto> i'm not sure i can improve the docs until (your) tomorrow morning, but my plan is to spend (my) whole morning doing that
```

## Unsorted logs from 2022-07-01

```
Day changed to 01 jul 2022
06:35  <rgdd> rhatto: overall it looks like you have nice documentation (on)going!
06:35  <rgdd> not gonna nit-pick, just some broader copy-pasted comments below
06:35  <rgdd> Project Information/:
06:35  <rgdd> RE: skills needed, with a future x509v3 ext we could remove "setting up DNS records"
06:35  <rgdd> might be worth adding under Applications/Requirements/The sauteed onion criteria that mentions ext
06:35  <rgdd> Applications/:
06:35  <rgdd> RE: domain seizure, it's already to late to take a binding down since CT logs are append-only!
06:35  <rgdd> RE: cert-based onion location, good observation about adv(2); that is probably a ~dead argument
06:35  <rgdd> RE: bi-directional associations; key rotiation; etc; _syverson has been looking in this direction before
06:35  <rgdd> related work ref for that: https://arxiv.org/pdf/2110.03168.pdf
06:35  <rgdd> RE: at the end, i'd strike out "But it remains undefined how..." paragraph; i'd draw the line as "not sauteed onions" with a blockchain
06:35  <rgdd> Service discovery/
06:35  <rgdd> RE: assumption 2, i don't think it is unreasonable to account for someone blocking your hosting in various ways
06:35  <rgdd> and that countries with exit nodes may unite to block certain sites (as is e.g. the case with the ongoing russia stuff)
06:35  <rgdd> it's a strong threat model though, no way to go around that
06:35  <rgdd> RE: analysis 4, that happens with today's onion location and sauteed onions too
07:35  <rgdd> i think the search engine angle falls apart a bit if you drop the strong censorship threat model
07:36  <rgdd> since you could just connect to the site and do discovery that way then (spinning of your docs rhatto)
07:36  <rgdd> a potential loss is that there has to be a connection to the regular site **and** the onion site tho
07:38  <rgdd> which is likely helpful for traffic analysis (you are touching the same thing in two places, regular site and onion site)
07:38  <rgdd> wrt. doing cert-based or header-based onion location (regardless of if we want some kind of verifiable search engine or not)
07:39  <rgdd> a certificate already provides machine for a TLS setting that is "not web" (i.e., with no HTTP headers)
07:39  <rgdd> the possibility of a site redirecting to many onion addresses could become visible (e.g., in an attempt to partition users)
07:39  <rgdd> it would also make it easy to detect if other sites redirect to your onion service (i guess that is useful information?)
07:40  <rgdd> i'm intentionally **excluding any potential extras** here to help us understand which things achieve what for whom
07:41  <rgdd> (i feel like that is the problem with something that is very entagled and potentially relating to many different things in subtle ways)
07:43  <rgdd> s/machine/machinery/
07:44  <rgdd> (and can easily lead one astray to not then look at each subtle problem individually instead)
09:13  <kwadronaut> when and where do i tune in for the presentation? :)
09:14  <rgdd> Friday July 1st at 1600 UTC
09:14  <rgdd> ROOM: https://tor.meet.coop/gab-dpb-9zt-7tq
09:14  <rgdd> kwadronaut: ^
09:14  <rgdd> (all hackweek presentations)
10:04  <rhatto> rgdd, thanks for the comments! i'm focusing in the docs/presentation and want to integrate them ASAP
10:05  <rhatto> if i have more time, i'd like to write a few remarks about usability and architectural discussion (#4 and #7)
10:05  <rgdd> rhatto: cool, keep up the good work!
10:06  <rhatto> i'm curious about the x509v3: i guess it would consist of some kind of record with the onion address, and the certification emission would include a step similar to what HARICA uses to emit certificates for .onions
                (generating a CSR from a nounce)
10:07  <rhatto> is something like that? i'd like to include a short summary about it in the docs, perhaps at the "Applications/Requirements/The sauteed onion criteria" section
10:08  <rgdd> something like that yes; to get the property of a unidirectional association it just needs to be there though, which is different compared to a .onion cert with harica
10:09  <rgdd> encoding the exact same information that we're doing now, just in a different place in the certificate basically (and still with the criteria that it would be invalid if example.com is not domain validated)
10:10  <rgdd> if you were to have that together with a .onion address, it would be an explicit mutual association (i.e., bi-directional)
10:11  <rgdd> (this is why it is so easy to make working now by adding an additional SAN; the same behavior can be modelled.)
10:12  <rhatto> bi-directionally would then be a plus over the current setup, and built in the extension... interesting!
10:12  <rgdd> can add another interesting future thing to think about
10:13  <rgdd> how many sites are there would onion location right now?
10:13  <rgdd> and how is that changing over time?
10:13  <rgdd> it says something about an approach like
10:13  <rhatto> wow, good question!
10:13  <rgdd> "just deliver the entire sauteed onion data set to the browser, similar to securedrop names"
10:13  <rgdd> the fun part is that its a good question independent of sauteed onions though :)
10:14  <rgdd> and its relatively straight forward to answer
10:14  <rgdd> since we have the domains to visit via ct logs :)
10:14  <rgdd> (i am obv gonna mention this in the future work section of the refactored paper btw :p)
10:14  <rgdd> probably the most interesting parameter for any potential list-based approaches
10:16  <rgdd> (upper bound i should say; the sauteed onion data set is currently like what; 4? :D)
10:37  <kwadronaut> rgdd: thanks!
10:51  <rhatto> rgdd, i think i addressed most of your comments at https://gitlab.torproject.org/rhatto/sauteed-week/-/commit/91c8c52f16e8c2660670ba108e832125dee65031
10:52  <rhatto> the search discussion remains to be integrated, along with lots of other stuff discussed this week
10:52  <rhatto> i think i also addressed the usability discussion (check the updated "Service discovery" page)
10:56  <rhatto> i'll focus on wrapping up to the presentation, then including more discussion (or maybe some raw logs would be enough at this point?)
10:59  <rhatto> what could be included as highlights for the presentation?
11:00  <rhatto> (i have some items already)
11:39  <rgdd> rhatto: diff looks good, and overall i think you've done an excellent job at capturing what's been happening in our irc log
11:39  <rgdd> regarding highlights, hmm
11:39  <rgdd> i guess there's like "the stuff i find the most interesting" and "the stuff that is reasonable to talk about in a few minutes"
11:40  <rgdd> i would probably put a large emphasis on the search engine angle to keep it concrete and close to what you hacked on
11:41  <rgdd> and mention something like, it appears that the search engine angle mostly brings to the table a strong cencorship model and it could allow the property of not visiting the regular site at all (go straight to onion site)
11:41  <rgdd> and also mention that there are a bunch of related/future work that hackweek was to short to look into
11:42  <rgdd> <insert your favorite thing or things to mention :p>
11:43  <rgdd> if you'd like to bring up a "catch", you could mention that it was identified as paramount to not just have SAN: <addr>onion.exmaple.com as a criteria, but also require example.com due to dangerous labels
11:43  <rgdd> and the potential future direction of an x509v3 extension and why that might be nice (e.g., no dns record setup)
11:44  <rgdd> i assume you will make some kind of demo!
11:44  <dkg> the more i think about it, the more i like the idea of slurping all valid certificates with "SyverSANs" into the Tor DHT (i don't know how you index the non-expired ones by the non-onion suffix, but that seems like it'd also
             be necessary)
11:44  <rgdd> dkg: same!
11:44  <dkg> then you can just have a CT monitor (or several independent ones) whose job it is to identify those certs, and you've got a transport mechanism and discovery mechanism already available.
11:45  <dkg> i will unfortunately miss the presentation, but thanks for working on and presenting this, rhatto!
11:48  <dkg> rgdd: it occurs to me that if you're dropping SyverSANs into the DHT, you could *also* drop DNSSEC-signed _onion TXT RRsets (at least, those with a reasonable validity window) into the DHT
11:49  <dkg> they'd have the same semantics, but a different cryptographic authority
11:49  <rgdd> dkg: yep, agreed! i think that would be a more suitable approach if the UX of requiring DNSSEC for site operators is accepted as a trade-off
11:50  <dkg> for the bundle DNSSEC bundle, you might want to include the full chain all the way up to the DNSSEC root
11:50  <dkg> so that they're self-validating for anyone who already knows the DNSSEC root
11:52  <dkg> i realize that i don't know enough about the Tor DHT to know (a) how you deal with it being spammed by someone who wants to spam it with a bunch of useless records, or (b) how the client can do a lookup based only on the
             normal DNS name.
11:56  <rgdd> dkg: those details are fuzzy for me too, but it feels like you would want to bind this together in some way so that your onion service descriptor must point at the dns name and vice versa at least; so that you'd get like "at
              most O(1) more records into the DHT and piggy-back on the existing model for spamming onion service description" (whatever that is)
11:57  <rgdd> and exactly how you would try to make this fit into the DHT (if it even does fit in a reasonable way) would be the 'future work' :p
11:58  <rgdd> but at least it seems like we do have alternatives for what records should go in there, question is then how
12:29  <rhatto> new/updated DEMO page: http://sauteef2orqgllvzy3cuspmaficgttf47rpbk24j555qcrhjc4duixid.onion/demo/
12:29  <rhatto> (needs some help with the questions and challenges sections)
12:33  <rgdd> rhatto: (2) is not a question, we know how to solve that with both CA/CT and DNSSEC even though the solutions differ on a technical level
12:34  <rhatto> rgdd, ok, shall i move it to challenges then?
12:34  <rgdd> i don't really consider that a challenge
12:35  <rhatto> ok, i'll remove that from the demo session and leave it to be include somewhere else
12:35  <rgdd> it's not much harder than maintaing a list; then telling a library to represent that list as a data structure. since the sauteed onion data set is so small (and will likely continue to be that for a forseable future), you
              can probably do this every time on startup in memory
12:35  <rgdd> it's more like future enhancement of your prototype
12:35  <rgdd> with regards to implementing
12:36  <rgdd> (which doesn't seem like a crazy thing to mention; "right now i have a data set - i can represent that with a data structure that gives me additional properties but i didn't have time")
12:38  <rgdd> i like question 1
12:39  <rgdd> you could also ask if resolvers could be part of TB (delivering the data set to be queried locally, similar to Onion Names)
12:39  <rgdd> as question 2
12:39  <rgdd> - how large wuold that data set be? what is the number of sites with onion location?
12:40  <dkg> number of sites with *both* an onion address *and* a non-onion address
12:42  <rgdd> (and i guess that estimate is also interesting for the DHT approach, as that would be a minimum bound of the overhead added to the DHT if it fits)
12:45  <rhatto> ok, i refactored the questions/challenges into a single item... http://sauteef2orqgllvzy3cuspmaficgttf47rpbk24j555qcrhjc4duixid.onion/demo/ (if you have it already opened it should have refreshed automatically, thanks to
                "mkdocs serve" running inside a docker container)
12:46  <rgdd> zooming out from those two questions; they both relate to the question of "what's next", and the answer is kinda "let's see" which is unsatisifying
12:46  <rhatto> anything else i should include before taking the last 15 minutes for rehearse the presentation? :)
12:47  <rgdd> checking
12:49  <rgdd> rhatto: i think that looks like a great presentation, take a break before you have to present :)
12:58  <rhatto> rgdd, thanks! 2-min break! :)
12:59  <rhatto> its just too much for 5 minutes, but i'll try
13:00  <rgdd> best-effort is good enough!
14:22  <rgdd> good work again rhatto, evening here so time for me to tune into weekend stuff! will be back monday but if lucky i'll find a terminal earlier than that :)
15:20  <rhatto> rgdd, thanks! it was an intense week... i also thank you and the others: arma2 dkg kwadronaut micah and _syverson for all the help :)
15:20  <rhatto> we had interesting discussions and i hope that this hackweek project contributed (even if a bit) to move this project forward
15:21  <rhatto> speaking of that, i just created two minor merge requests: https://gitlab.torproject.org/rgdd/sauteed-onions/-/merge_requests
15:29  <kwadronaut> i'm glad that my lurking eyes were seen in a positive way :)
```
