# Sauteed Week

A [project][] to investigate [Sauteed Onions][] during the [Tor Hackweek][]
2022.

[project]: https://gitlab.torproject.org/rhatto/sauteed-week
[Sauteed Onions]: https://www.sauteed-onions.org/
[Tor Hackweek]: https://gitlab.torproject.org/tpo/community/hackweek
