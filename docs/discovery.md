# Discovery strategies

This document discusses discovery strategies so the
[Sauteed Onions](https://www.sauteed-onions.org/) scheme can be considered
beyond CT Logs.

## Assumptions

1. User already has Tor Browser installed and running (either vanilla or using
   pluggable transports). Sauteed Onions does not help if the user cannot
   access the Tor network.
2. It's reasonable to account for someone blocking hosting in various ways
   and that countries where Tor exit nodes exist may unite to block certain sites.
   This means that assumption **1** does not imply that a user accessing the Tor
   network won't have problems accessing a site. This is then an attempt to use
   a _strong censhorship_ model into the analysis.
3. CT Logs are hard to censor (as they use is enforced by some major browsers).

## UX (user experience) and AX (admin expertise)

We could imagine something like that:

1. Sysadmins publishes the .onion address for a given domain under various
   ways (as many as possible), such as:
    * a. Onion-Location HTTP Header.
    * b. DNS (with DNSSEC in a custom TXT format or something like DANE).
    * c. Sauteed Onions (Subject Alt-Name in a certificate).
2. Clients using the Tor Browser tries fetch (or connect to) the corresponding
   .onion address of a given site by incremental attempts using different methods:
    * a. Sauteed Onions by trying to fetch the certificate directly:
        * If a direct HTTPS connection is sucessful:
            * If a valid certificate is found, check the SCTs. Refuse to connect
              with unvalid certs of with valid certs without valid SCTs.
            * If an .onion address is found as a sauteed onion SAN, activate
              the Onion Location button in the address bar.
    * a. Looking for the Onion-Location HTTP Header:
        * If a direct HTTPS connection is sucessful:
            * If a valid certificate is found, check the SCTs. Refuse to connect
              with unvalid certs of with valid certs without valid SCTs.
            * If an Onion-Location Header is found, activate the Onion Location
              button in the address bar (that's the current, implemented behavior).
    * c. Sauteed Onions via a resolver (which in the backend monitors the CT Logs):
        * Either in case of a successful (valid cert and SCTs) or unsucessful
          HTTPS connection to the site (invalid cert, invalid/non-existent
          SCTs, or no connection at all):
            * Try to get/validate the onion address from a resolver API based on
              CT Logs.
        * If the connection is sucessful and an .onion address is
          found/validated via the resolver API method, present the Onion
          Location button in the address bar.
        * If the connection is unsucessful but an .onion address is
          found/validated via the resolver API method, redirect the user to the
          .onion address.
    * d. Checking the DNS:
        * Either in case of a successful (valid cert and SCTs) or unsucessful
          HTTPS connection to the site (invalid cert, invalid/non-existent
          SCTs, or no connection at all):
            * Try to get/validate the onion address from an (signed) DNS record.
        * If the connection is sucessful and an .onion address is
          found/validated via the DNS method, present the Onion Location button in the
          address bar.
        * If the connection is unsucessful but an .onion address is
          found/validated via the DNS method, redirect the user to the .onion
          address.

As an analogy with [Pluggable Transports][], this could be understood as some
sort of `Pluggable Discoverability` (or `Opportunistic Discoverability`).

This incremental approach allows clients to try first what's cheaper to compute
and connect, falling back to more sofisticated methods.

[Pluggable Transports]: https://www.pluggabletransports.info/

## Analysis

Then, with those strategies summarized above, we have that:

1. Only option `2.c`. really needs a custom resolver API. All the other approaches
   can be executed directly by the client.

2. The API might return not only the .onion address for a site but also the
   current certificate, as a way to properly check (against the existing CAs in
   the client application) if the .onion address is really controlled by those
   controlling the DNS domain.

3. The API could be available both behind .onion services "regular" (non-onion
   service) endpoints, so it would not be limited only to Tor Browser users.

4. The pure-DNS and the Onion-Location Header approaches (i.e, without
   involving onion addresses included directly in the TLS certs) has the
   following disadvantage: domain operators can create an association between
   their domains to _any_ Onion Service, so they can claim the association without
   actually controlling the Onion Service. This does not happen with the
   approaches based on TLS certificates, as they're at least Domain-validated
   (DV).
