# Onion Service address discovery using Sauteed Onions - Tor Hackweek 2022

## About

[Sauteed Onions][] can be used for censorship circumvention and may also serve as another way of doing [Onion Names][]. This [Tor Hackweek][] 2022 project tries to contribute to the ongoing research.

## Where to meet

* IRC: `#sauteed-onions` on [OFTC][] (also available as a [Matrix bridge][])
* [Sync meeting room][]
* [Project pad][]
* [Gitlab project][]

## How to join

Just join the IRC channel and add your (nick)name in the section below :)

## Goals

* Having a proof-of-concept of the full circle would be a good (but maybe
  ambitious) roadmap for the week.

## Proposed activities

See the [issue board][]

## Roadmap

0. Bootstrap (until June 27th).
1. Create a Sauteed Onion certificate ([#1][]) (until June 28th).
2. Complete the reading list ([#6][]) (until June 29th).
3. Play a bit more with the API and then tinker with code ([#2][] and [#3][])
   (until June 30th).
4. Document and discuss in parallel ([#5][], [#7][] and [#4][]) (until July 1st).

## Skills needed

What we'll do really depends on the aggregated skills in the team, such as:

* System administration skills for test and deploy Sauteed Onions by creating
  Onion Services for existing sites, setting the Sauteed Onion DNS record (with a
  future x509v3 extension the DNS record not be needed, see [the sauteed onion
  criteria][] for info) and generating TLS certificates with the Sauteed Onion
  Subject Alt Name for the site.

* Coding skills depending on the tools to be developed.

* UX skills.

* People with experience with CT Logs :)

## References

* [Initial ideas][]
* [Hackweek project page][]
* [Sauteed Onions GitLab project][]

[Sauteed Onions]: https://www.sauteed-onions.org
[Tor Hackweek]: https://gitlab.torproject.org/tpo/community/hackweek
[Onion Names]: https://securedrop.org/news/introducing-onion-names-securedrop/
[OFTC]: https://www.oftc.net/
[Matrix bridge]: https://matrix.org/docs/guides/types-of-bridging
[issue board]: https://gitlab.torproject.org/rhatto/sauteed-week/-/boards
[Sync meeting room]: https://tor.meet.coop/sil-nju-qa4-ils
[Project pad]: https://pad.riseup.net/p/tor-hackweek-2022-sauteed-onions-keep
[GitLab project]: https://gitlab.torproject.org/rhatto/sauteed-week
[Initial ideas]: https://gitlab.torproject.org/tpo/onion-services/onion-support/-/issues/116
[Hackweek project page]: https://hackweek.onionize.space/hackweek/talk/GLMQVT/
[Sauteed Onions GitLab project]: https://gitlab.torproject.org/rgdd/sauteed-onions
[#1]: https://gitlab.torproject.org/rhatto/sauteed-week/-/issues/1
[#2]: https://gitlab.torproject.org/rhatto/sauteed-week/-/issues/2
[#3]: https://gitlab.torproject.org/rhatto/sauteed-week/-/issues/3
[#4]: https://gitlab.torproject.org/rhatto/sauteed-week/-/issues/4
[#5]: https://gitlab.torproject.org/rhatto/sauteed-week/-/issues/5
[#6]: https://gitlab.torproject.org/rhatto/sauteed-week/-/issues/6
[#7]: https://gitlab.torproject.org/rhatto/sauteed-week/-/issues/7
[the sauteed onion criteria]: /applications/#the-sauteed-onion-criteria
