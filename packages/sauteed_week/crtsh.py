#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Interaction with crt.sh.
#
# Copyright (C) 2022 Silvio Rhatto <rhatto@torproject.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# WARNING: this should only be used for testing purposes, and not for
# wholesale querying of crt.sh.

import requests
import feedparser
import re
import json
import os

from bs4          import BeautifulSoup
from cryptography import x509

# The base path for this project
basepath = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, os.pardir) + os.sep

search_query_string      = 'https://crt.sh/atom?identity={addr}&match=='
get_query_string         = 'https://crt.sh/?id={id}'
certificate_query_string = 'https://crt.sh/?d={id}'
sauteed_regexp           = re.compile(r'[a-z2-7]{56}onion\.')
id_regexp                = re.compile(r'\?id=(?P<id>[0-9]*)#')
cert_regexp              = re.compile(
        r'-----BEGIN CERTIFICATE-----(?P<cert>[^\-]*)-----END CERTIFICATE-----',
        re.MULTILINE)

def load_log_list():
    """
    Load the list of known CT Logs.

    Ideally this needs to be done periodically and with signature checking.

    For now we'll rely in a local file.

    See https://certificate.transparency.dev/google/
    """

    log_file = os.path.join(basepath, 'configs', 'log_list.json')

    with open(log_file) as contents:
        return json.load(contents)

def get_log_id(log_url):
    """
    Get a log_id given a log_url

    Maybe it's also possible to get the log ID with this:
    https://cryptography.io/en/latest/x509/certificate-transparency/

    """

    if log_url is None or log_url == '':
        return False

    log_list = load_log_list()

    for operator in log_list['operators']:
        for log in operator['logs']:
            # Ugly hack since crt.sh is using "ct-fixed-ip.googleapis.com", a
            # domain currently not in the log_list
            log['url'] = log['url'].replace('https://ct.googleapis.com', 'https://ct-fixed-ip.googleapis.com')

            if log['url'].strip('/') == log_url.strip('/'):
                return log['log_id']

    return False

def get_info_from_cert(id, pem):
    """
    Gets information from a PEM certificate and a crt.sh ID

    Might need an additional protection against null-prefix attacks:
    http://www.thoughtcrime.org/papers/null-prefix-attacks.pdf

    """

    if pem is None or pem == '':
        return False

    if id is None or id == 0:
        return False

    cert       = x509.load_pem_x509_certificate(bytes(pem, 'utf-8'))
    ext        = cert.extensions.get_extension_for_oid(x509.ExtensionOID.SUBJECT_ALTERNATIVE_NAME)
    #addr      = cert.subject.get_attributes_for_oid(x509.NameOID.COMMON_NAME)[0]['value']
    addr       = ''
    sans       = ext.value.get_values_for_type(x509.DNSName)
    onion_addr = ''

    for san in sans:
        match = sauteed_regexp.search(san)

        if match:
            onion_addr = match[0].replace('onion.', '.onion')
            addr       = san[62:]

            return {
                    'domain_name': addr,
                    'onion_addr' : onion_addr,
                    'identifiers': [
                        id,
                        ],
                    }

    return False

def get_ct_log_entries(id):
    """
    Gets CT Log entry information from a crt.sh ID

    """

    if id is None:
        return False

    response  = requests.get(get_query_string.format(id=id)).text
    soup      = BeautifulSoup(response, 'html.parser')
    tables    = soup.find_all('table')
    log_table = tables[2].find('tr').find('td').find('table').find_all('tr')
    entries   = log_table[2:]
    results   = []

    for entry in entries:
        cols = entry.find_all('td')

        results.append({
            'timestamp': cols[0].get_text(),
            'log_index': cols[1].get_text(),
            'log_url'  : cols[3].get_text(),
            })

    return results

def search(addr):
    """
    Implements the /search method

    Still needs proper checking, such as certificate and SCTs validations.

    See https://gitlab.torproject.org/rgdd/sauteed-onions/-/blob/main/monitor/doc/api.md
    """

    if addr is None or addr == '':
        return False

    try:
        response = feedparser.parse(search_query_string.format(addr=addr))

        # This is somehow heuristic: assumes that the newest cert is the last one in the resultset.
        # Dates should be tested instead.
        entry = response.entries[-1]

        if not hasattr(entry, 'summary'):
            return False

        id      = id_regexp.search(entry.id)['id']
        summary = entry.summary.replace('<br />', "\n")
        soup    = BeautifulSoup(summary, 'html.parser')
        parsed  = cert_regexp.search(soup.get_text())

        if parsed:
            data = parsed.groupdict()
        else:
            return False

        if 'cert' not in data:
            return False

        pem  = "-----BEGIN CERTIFICATE-----\n" + data['cert'] + '-----END CERTIFICATE-----'
        info = get_info_from_cert(id, pem)

    except Exception:
        return False

    return info

def get(id):
    """
    Implements the /get method

    Still needs proper checking, such as certificate and SCTs validations.

    See https://gitlab.torproject.org/rgdd/sauteed-onions/-/blob/main/monitor/doc/api.md
    """

    if id is None or id == 0:
        return False

    try:
        pem     = get_certificate(id)
        info    = get_info_from_cert(id, pem)
        entries = get_ct_log_entries(id)

        del info['identifiers']

        # Always giving the first result of the 'entries' list
        # That could be randomized to offer other lists
        # Or the API could be changed to allow more than one CT Log to be listed
        info.update({
          "log_id"     : get_log_id(entries[0]['log_url']),
          "log_index"  : entries[0]['log_index'],
          "cert_path"  : "/get-certificate?id={id}".format(id=id),
        })

    except Exception:
        return False

    return info

def get_certificate(id):
    """
    Get a certificate from crt.sh.

    """

    if id is None or id == 0:
        return False

    try:
        pem    = requests.get(certificate_query_string.format(id=id)).text
        parsed = cert_regexp.search(pem)

        if not parsed:
            return False

    except Exception:
        return False

    return pem
