#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Example API for Sauteed Onions.
#
# Copyright (C) 2022 Silvio Rhatto <rhatto@torproject.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This is conceptual software and should not be used in production.
# It's the result of a single hackweek of experimentation :)
#
# See https://gitlab.torproject.org/rgdd/sauteed-onions/-/blob/main/monitor/doc/api.md

from typing            import Union
from fastapi           import FastAPI, Query
from fastapi.responses import RedirectResponse, StreamingResponse
from io                import StringIO

import crtsh

app = FastAPI(
        title="Sauteed Week",
        version="0.0.1",
        description="""Provides a test API for [Sauteed Onions](https://www.sauteed-onions.org) based on
                       it's [specs](https://gitlab.torproject.org/rgdd/sauteed-onions/-/blob/main/monitor/doc/api.md).
                       It currently uses [crt.sh](https://crt.sh) as the backend and has a very simple implementation,
                       with lots of assumptions, heuristics and hacks. Use it only for research and testing.""",
        contact={
            'name': 'Sauteed Week Team',
            'url': "https://gitlab.torproject.org/rhatto/sauteed-week",
            },
        )

@app.get("/")
async def redirect_typer():
    """
    The [Sauteed onion search API specs][] does not define what should live in
    the `/` (index) endpoint.

    So this action just redirect users to the API documentation page.

    [Sauteed onion search API specs]: https://gitlab.torproject.org/rgdd/sauteed-onions/-/blob/main/monitor/doc/api.md
    """

    return RedirectResponse("/docs")

# Need to use an alias param for the 'in' parameter
# See https://fastapi.tiangolo.com/tutorial/query-params-str-validations/
@app.get("/search")
def read_item(addr: Union[str, None] = Query(alias="in")):
    """
    Implements the `/search` method

    Input:

    - `in`: domain name to look up onion addresses for (exact match)

    Output:

    - A list with zero or more objects with the following key-value pairs:
      - `domain_name`: string (matches what was specified by `in`)
      - `onion_addr` : string (an onion address associated with `domain_name`)
      - `identifiers`: list (unique identifiers to access auxiliary
                       certificate information)

    Known search strings that returns Sauteed Onions certificates:

    - [www.sauteed-onions.org][]
    - [autodefesa.org][]
    - [www.rgdd.se][]
    - [sysrqb.xyz][]

    For details, check the [Sauteed onion search API specs][]

    [www.sauteed-onions.org]: /search?in=www.sauteed-onions.org
    [autodefesa.org]: /search?in=autodefesa.org
    [www.rgdd.se]: /search?in=www.rgdd.se
    [sysrqb.xyz]: /search?in=sysrqb.xyz
    [Sauteed onion search API specs]: https://gitlab.torproject.org/rgdd/sauteed-onions/-/blob/main/monitor/doc/api.md
    """

    return crtsh.search(addr)

@app.get("/get")
def read_item(id: int):
    """
    Implements the `/get` method

    Input:

    - `id`: an identifier to locate auxiliary certificate information

    Output:

    - `domain_name`: string
    - `onion_addr` : string (an onion address associated with `domain_name`)
    - `log_id`     : string (defined in [RFC 6962, §3.2][]; base64-encoded)
    - `log_index`  : uint64 (log index to locate the sauteed onion certificate)
    - `cert_path`  : string (relative path to `url` for downloading the sauteed
                     onion certificate)

    For details, check the [Sauteed onion search API specs][]

    [RFC 6962, §3.2]: https://datatracker.ietf.org/doc/html/rfc6962#section-3.2
    [Sauteed onion search API specs]: https://gitlab.torproject.org/rgdd/sauteed-onions/-/blob/main/monitor/doc/api.md

    """

    return crtsh.get(id)

@app.get("/get-certificate")
def read_item(id: int):
    """
    Implements the `/get-certificate` method

    Input:

    - `id`: an identifier to locate auxiliary certificate information

    This action is not defined in the current spec but an `/example-path` to download
    the cert is.

    For details, check the [Sauteed onion search API specs][]

    [Sauteed onion search API specs]: https://gitlab.torproject.org/rgdd/sauteed-onions/-/blob/main/monitor/doc/api.md
    """

    pem = crtsh.get_certificate(id)

    if pem != False:
        return StreamingResponse(StringIO(pem), media_type="application/pkix-cert")

    return False
