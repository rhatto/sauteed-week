#
# Sauteed Week Makefile.
#
# This Makefile is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3 of the License, or any later version.
#
# This Makefile is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA 02111-1307, USA
#

#
# Settings
#

MAIN_PAD = https://pad.riseup.net/p/tor-hackweek-2022-sauteed-onions-keep
WORKERS  = 10

.PHONY: docs

#
# Documentation
#

ONION_MKDOCS_PATH ?= vendors/onion-mkdocs

# This is useful when developing your documentation locally and Onion MkDocs is
# not yet installed on your project but you don't want it to be a Git submodule.
#
# If you use this approach, make sure to at the Onion MkDocs path into your
# .gitignore.
vendoring:
	@test   -e $(ONION_MKDOCS_PATH) && git -C $(ONION_MKDOCS_PATH) pull || true
	@test ! -e $(ONION_MKDOCS_PATH) && git clone https://gitlab.torproject.org/tpo/web/onion-mkdocs.git $(ONION_MKDOCS_PATH) || true

# Include the Onion MkDocs Makefile
# See https://www.gnu.org/software/make/manual/html_node/Include.html
-include $(ONION_MKDOCS_PATH)/Makefile.onion-mkdocs

# Requires a compatible export-pad script,
# such as https://git.fluxo.info/scripts/tree/export-pad
pad:
	@export-pad "$(MAIN_PAD)" "docs/info.md"

slides:
	@make -C docs/slides

docs:
	@vendors/onion-mkdocs/scripts/onion-mkdocs-build

serve:
	@vendors/onion-mkdocs/scripts/onion-mkdocs-serve

#
# API
#

vendor:
	@pipenv install

api:
	@pipenv run uvicorn api:app --app-dir packages/sauteed_week \
		--reload --host 0.0.0.0 --port 8100 \
		--workers $(WORKERS)

#
# Containers
#

run-containers:
	@docker-compose up -d

watch-containers:
	@watch docker-compose ps

log-containers:
	@docker-compose logs -f

stop-containers:
	@docker-compose down
