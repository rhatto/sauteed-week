# Onion Service address discovery using Sauteed Onions - Tor Hackweek 2022

## About

This is the repository for the 2022
[Tor Hackweek](https://gitlab.torproject.org/tpo/community/hackweek)
project about [Sauteed Onions](https://www.sauteed-onions.org/).

* [Hackweek project page](https://hackweek.onionize.space/hackweek/talk/GLMQVT/).
* [Project pad](https://pad.riseup.net/p/tor-hackweek-2022-sauteed-onions-keep),
  sometimes synced [here](docs/info.md).
* [GitLab project](https://gitlab.torproject.org/rhatto/sauteed-week).

## Documentation

* See the [live documentation site](https://rhatto.pages.torproject.net/sauteed-week/).
* Check the [presentation slides](docs/slides/presentation.pdf) for an intro.
* Check [other docs](docs) for details.

## Credits

* This repository was built from collective discussions and contributions.
* Special thanks to:
  * Those at the `#sauteed-onions` channel on OFTC (just ask if you want to be
    explicitly mentioned here).
  * The [Sauteed Onions team](https://www.sauteed-onions.org/) by their support
    and encouragement.
